<?php

function getUrlData($url, $raw=false) // $raw - enable for raw display
{
    $result = false;

    $contents = getUrlContents($url);

    if (isset($contents) && is_string($contents))
    {
        $title = null;
        $metaTags = null;
        $metaProperties = null;

        preg_match('/<title.*?>([^>]*)<\/title>/si', $contents, $match );

        if (isset($match) && is_array($match) && count($match) > 0)
        {
            $title = strip_tags($match[1]);
        }

        preg_match_all('/<[\s]*meta[\s]*?.*?(name|property)="?' . '([^>"]*)"?[\s]*' . 'content="?([^>"]*)"?[\s]*[\/]?[\s]*>/si', $contents, $match);
        //print_r($match);
        if (isset($match) && is_array($match) && count($match) == 4)
        {
            $originals = $match[0];
            $names = $match[2];
            $values = $match[3];

            //print_r($names);
            //print_r($values);
            //print_r($originals);
            if (count($originals) == count($names) && count($names) == count($values))
            {
                $metaTags = array();
                $metaProperties = $metaTags;
                if ($raw) {
                    if (version_compare(PHP_VERSION, '5.4.0') == -1)
                         $flags = ENT_COMPAT;
                    else
                         $flags = ENT_COMPAT | ENT_HTML401;
                }

                for ($i=0, $limiti=count($names); $i < $limiti; $i++)
                {
                    if ($match[1][$i] == 'name')
                         $meta_type = 'metaTags';
                    else
                         $meta_type = 'metaProperties';
                    if ($raw)
                        ${$meta_type}[$names[$i]] = array (
                             $values[$i]
                        );
                    else
                        ${$meta_type}[$names[$i]] = array (
                            $values[$i]
                        );
                }
            }
        }

        $result = array (
            'title' => $title,
            'metaTags' => $metaTags,
            'metaProperties' => $metaProperties,
        );
        //print_r($result);
    }
    return $result;
}

function getUrlContents($url, $maximumRedirections = null, $currentRedirection = 0)
{
    $result = false;

    $contents = @file_get_contents($url);
    //print($contents);
    // Check if we need to go somewhere else

    if (isset($contents) && is_string($contents))
    {
        preg_match_all('/<[\s]*meta[\s]*http-equiv="?REFRESH"?' . '[\s]*content="?[0-9]*;[\s]*URL[\s]*=[\s]*([^>"]*)"?' . '[\s]*[\/]?[\s]*>/si', $contents, $match);

        if (isset($match) && is_array($match) && count($match) == 2 && count($match[1]) == 1)
        {
            if (!isset($maximumRedirections) || $currentRedirection < $maximumRedirections)
            {
                return $this->getUrlContents($match[1][0], $maximumRedirections, ++$currentRedirection);
            }

            $result = false;
        }
        else
        {
            $result = $contents;
        }
    }

    return $contents;
}
function Zip($source, $destination)
{
    if (!extension_loaded('zip') || !file_exists($source)) {
        return false;
    }

    $zip = new ZipArchive();
    if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
        return false;
    }

    $source = str_replace('\\', '/', realpath($source));

    if (is_dir($source) === true)
    {
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

        foreach ($files as $file)
        {
            $file = str_replace('\\', '/', $file);

            // Ignore "." and ".." folders
            if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                continue;

            $file = realpath($file);

            if (is_dir($file) === true)
            {
                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
            }
            else if (is_file($file) === true)
            {
                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
            }
        }
    }
    else if (is_file($source) === true)
    {
        $zip->addFromString(basename($source), file_get_contents($source));
    }

    return $zip->close();
}
function zipData($source, $destination) {
	if (extension_loaded('zip')) {
		if (file_exists($source)) {
			$zip = new ZipArchive();
			if ($zip->open($destination, ZIPARCHIVE::CREATE)) {
				$source = realpath($source);
				if (is_dir($source)) {
					$iterator = new RecursiveDirectoryIterator($source);
					// skip dot files while iterating
					$iterator->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);
					$files = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::SELF_FIRST);
					foreach ($files as $file) {
						$file = realpath($file);
						if (is_dir($file)) {
							$zip->addEmptyDir(str_replace($source . '', '', $file . ''));
						} else if (is_file($file)) {
							$zip->addFromString(str_replace($source . '', '', $file), file_get_contents($file));
						}
					}
				} else if (is_file($source)) {
					$zip->addFromString(basename($source), file_get_contents($source));
				}
			}
			return $zip->close();
		}
	}
	return false;
}

function imageResize($imageSrc,$imageWidth,$imageHeight,$new_size) {

    $newImageWidth =$new_size;
    $newImageHeight =$new_size;

    $newImageLayer=imagecreatetruecolor($newImageWidth,$newImageHeight);
    imagecopyresampled($newImageLayer,$imageSrc,0,0,0,0,$newImageWidth,$newImageHeight,$imageWidth,$imageHeight);

    return $newImageLayer;
}


function createZip($zip,$dir){
  if (is_dir($dir)){

    if ($dh = opendir($dir)){
       while (($file = readdir($dh)) !== false){
 
         // If file
         if (is_file($dir.$file)) {
            if($file != '' && $file != '.' && $file != '..'){
 
               $zip->addFile($dir.$file);
            }
         }else{
            // If directory
            if(is_dir($dir.$file) ){

              if($file != '' && $file != '.' && $file != '..'){

                // Add empty directory
                $zip->addEmptyDir($dir.$file);

                $folder = $dir.$file.'/';
 
                // Read data of the folder
                createZip($zip,$folder);
              }
            }
 
         }
 
       }
       closedir($dh);
     }
  }
}

function send_notification($payload,$title,$message,$include_image='',$regId)
{
    error_reporting(-1);
    ini_set('display_errors', 'On');

    require_once 'firebase/firebase.php';
    require_once 'firebase/push.php';   

    $firebase = new Firebase();
    $push = new Push();

    // optional payload
    // $payload = array();
    // $payload['team'] = 'India';
    // $payload['score'] = '5.6';

    // notification title
    // $title = "testing";
    
    // notification message
    // $message = "Testing Message First ";
    
    // push type - single user / topic
    $push_type = 'individual';
    
    // whether to include to image or not
    $include_image = isset($_GET['include_image']) ? TRUE : FALSE;


    $push->setTitle($title);
    $push->setMessage($message);
    if ($include_image) {
        $push->setImage($include_image);
    } else {
        $push->setImage('');
    }
    $push->setIsBackground(FALSE);
    $push->setPayload($payload);


    $json = '';
    $response = '';

    if ($push_type == 'topic') {
        $json = $push->getPush();
        $response = $firebase->sendToTopic('global', $json);
    } else if ($push_type == 'individual') {
        $json = $push->getPush();
        $response = $firebase->send($regId, $json);
    }

    return $response;
}

function send_notification_testing()
{
    error_reporting(-1);
    ini_set('display_errors', 'On');

    require_once 'firebase/firebase.php';
    require_once 'firebase/push.php';   

    $firebase = new Firebase();
    $push = new Push();

    // optional payload
    $payload = array();
    $payload['team'] = 'India';
    $payload['score'] = '5.6';

    // notification title
    $title = "testing";
    
    // notification message
    $message = "Testing Message First ";
    
    // push type - single user / topic
    $push_type = 'individual';
    
    // whether to include to image or not
    $include_image = isset($_GET['include_image']) ? TRUE : FALSE;


    $push->setTitle($title);
    $push->setMessage($message);
    if ($include_image) {
        $push->setImage('http://api.androidhive.info/images/minion.jpg');
    } else {
        $push->setImage('');
    }
    $push->setIsBackground(FALSE);
    $push->setPayload($payload);


    $json = '';
    $response = '';

    if ($push_type == 'topic') {
        $json = $push->getPush();
        $response = $firebase->sendToTopic('global', $json);
    } else if ($push_type == 'individual') {
        $json = $push->getPush();
        $regId = 'eDtz8Jyz3L8AozMzw3Vrdu:APA91bHR8dS_dUrTBsnn1mPjz1EL91O7jfpkziXrnUHPEirC9YfMaTb3zMracgwZqk0SPtWtB0ka6om8t1n0g23orl04t9qoNdGZexmUSRWW3L6mabDgvO0nu5pSAwzwU-Pk9Ejry5bx';
        $response = $firebase->send($regId, $json);
    }

    return $response;
}


function sendEmail($subject,$message,$sending_email)
{    
    require_once(APPPATH."libraries/phpmailer/PHPMailerAutoload.php");
    //Create a new PHPMailer instance
    $mail = new PHPMailer;
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'email-smtp.us-east-1.amazonaws.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'AKIAYAK3QU4EFKVUD4F7';                 // SMTP username
    $mail->Password = 'BHpSxoYU9uvA+3sxc73+ztBmmPilyikNxeIzb3MBcxHW';                           // SMTP password
    //$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                 // TCP port to connect to
    $mail->Sender = 'support@stockilyapp.com';          // for return path
    
    //Set who the message is to be sent from
    $mail->setFrom('support@stockilyapp.com', 'Stockily');
    //Set an alternative reply-to address
    $mail->addReplyTo('support@stockilyapp.com', 'support@stockilyapp.com');
    //Set who the message is to be sent to
    $mail->addAddress($sending_email);
    //Set the subject line
    $mail->CharSet = 'UTF-8';
    $mail->Encoding = 'base64';
    $mail->Subject = $subject;
    $mail->isHTML(true);  // Set email format to HTML
    $mail->Body    = $message;
    //Replace the plain text body with one created manually
    $mail->AltBody = strip_tags($message);
    $mail->SMTPOptions = array(
        'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true,
        ));
    //send the message, check for errors
    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
            return true;
    }

}