<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->login_data = $this->session->userdata('login_data');
		$this->login_plan = $this->session->userdata('login_plan');
		if(function_exists('date_default_timezone_set')) {
			date_default_timezone_set("america/new_york");
		}
		if (!$this->login_data->id) {
			redirect('Login');
		}
		if ($this->login_data->is_admin == 1) {
			redirect('superadmin/user');
		}
		$this->current_date_time = date('Y-m-d H:i:s');
	}

	public function support()
	{
		$this->load->view('user/pages/support');
	}
	public function bonus()
	{
		$this->load->view('user/pages/bonus');
	}
	public function wordpress()
	{
		$this->load->view('user/pages/wordpress');
	}
	public function tutorial()
	{
		$this->load->view('user/pages/tutorial');
	}

}

/* End of file Pages.php */
/* Location: ./application/controllers/Pages.php */