<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->login_data = $this->session->userdata('login_data');
		$this->login_plan = $this->session->userdata('login_plan');
		if(function_exists('date_default_timezone_set')) {
		    date_default_timezone_set("america/new_york");
		}
		if (!$this->login_data->id) {
			redirect('Login');
		}
		if ($this->login_data->is_admin == 1) {
			redirect('superadmin/user');
		}
		$this->current_date_time = date('Y-m-d H:i:s');
	}

	public function index()
	{
		// echo "<pre>";
		// print_r($this->login_plan);
		// return;
		$this->load->model('Model_manifests');
		$this->load->model('Model_user_notification');
		$this->load->model('Model_notification_tokens');
		$data['count_myapp'] = $this->Model_manifests->count_userapp($this->login_data->id);
		$data['count_notification'] = $this->Model_user_notification->count_user_notification($this->login_data->id);
		$data['count_subscriber'] = $this->Model_notification_tokens->count_user_subscriber($this->login_data->id);
		$this->load->view('user/dashboard',$data);
	}

	public function form()
	{
		$this->load->view('form');
	}

	public function testing()
	{
		print_r(send_notification_testing());

	}

	public function superadmin()
	{
		if($this->session->userdata('login_data_superadmin'))
		{

			$login_data_superadmin = $this->session->userdata('login_data_superadmin');
			// $this->session->sess_destroy();
			$this->session->set_userdata('login_data',$login_data_superadmin);
			$this->session->set_userdata('login_data_superadmin','');
			redirect(base_url().'superadmin/user','refresh');
		}
	}

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */