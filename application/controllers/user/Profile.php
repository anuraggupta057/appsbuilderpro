<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->login_data = $this->session->userdata('login_data');
		$this->login_plan = $this->session->userdata('login_plan');
		if(function_exists('date_default_timezone_set')) {
			date_default_timezone_set("america/new_york");
		}
		if (!$this->login_data->id) {
			redirect('Login');
		}
		if ($this->login_data->is_admin == 1) {
			redirect('superadmin/user');
		}
		$this->current_date_time = date('Y-m-d H:i:s');
	}

	public function edit_profile()
	{
		$this->load->model('Model_login');
		$data['user'] = $this->Model_login->get_login_by_id_md5(md5($this->login_data->id));
		$this->load->view('user/profile/edit_profile');
		// print_r($data);
		
	}

	public function update_password()
	{
		$password = $_POST['password'];
		$this->load->model('Model_login');
		$data_array = array(
			'password'=>md5($password),
		);

		$data_id = $this->Model_login->update_login($data_array,$this->login_data->id);
		if ($data_id) {
			echo "Valid";
		}
	}


	public function update_profile()
	{
		
		$time = microtime();
		$item_img='';
		$img_name='';
		if($_FILES)
		{        	
			$info=pathinfo($_FILES['file1']['name']);            
			if($info!='')
			{
				$ext=$info['extension'];
				$item_img=$_FILES['file1']['name'];
				$target='files/user_img/'.$time.$item_img;
				move_uploaded_file($_FILES['file1']['tmp_name'],$target);
				$img_name=$time.$item_img;
			}        
		}
		$data_item = array(
			'name'=>$_POST['name'],
			'email'=>$_POST['email'],
			'country'=>$_POST['country'],
			'img'=>$img_name,		
		);
		$this->load->model('Model_login');
		$item_id = $this->Model_login->update_login($data_item,$this->login_data->id);	
		if ($item_id) {
			$login_data = $this->Model_login->get_login_by_id_md5(md5($this->login_data->id));
			$this->session->set_userdata('login_data',$login_data);
			$this->session->set_flashdata('Message','Profile Update Successfully');
			echo "Valid";
		}
		else
		{
			echo "Invalid";
		}
	}
}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */