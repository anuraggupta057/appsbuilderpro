<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apps extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->login_data = $this->session->userdata('login_data');
		$this->login_plan = $this->session->userdata('login_plan');
		if(function_exists('date_default_timezone_set')) {
			date_default_timezone_set("america/new_york");
		}
		if (!$this->login_data->id) {
			redirect('Login');
		}
		if ($this->login_data->is_admin == 1) {
			redirect('superadmin/user');
		}
		$this->current_date_time = date('Y-m-d H:i:s');
	}

	public function index()
	{
		$this->load->model('Model_manifests');
		$data['manifest'] = $this->Model_manifests->get_user_manifests($this->login_data->id);
		$this->load->view('user/apps/view_apps',$data);
	}

	public function create_app()
	{
		$this->load->view('user/apps/create_app');
	}

	public function generate_manifest()
	{
		$url = $_GET['url'];
		$data['url_data'] = getUrlData($url);


		// echo "<pre>";
		// print_r($data);
		// return;
		$this->load->view('user/apps/generate_manifest',$data);

	}

	public function manifest_store()
	{
		$name =$_POST['name'];
		$short_name = $_POST['shortname'];
		$description = $_POST['description'];
		$display = "Standalone";
		$start_url = $_POST['start_url'];
		$start_url = rtrim($start_url, '/');
		$gdpr = 0;
		$facebook = 0;
		$google = 0;

		if (isset($_POST['gdpr'])) {
			$gdpr=1;
		}
		if (isset($_POST['facebook'])) {
			$facebook=1;
		}
		if (isset($_POST['google'])) {
			$google=1;
		}

		$str = json_decode(file_get_contents( base_url().'files/manifest.json'));
		$str->name = $name;
		$str->short_name = $short_name;
		$str->description = $description;

		$json_e=json_encode($str);
		$icon= $str->icons;

		$this->load->model('Model_manifests');
		$data_array = array(
			'url'=>$start_url,
			'gdpr'=>$gdpr,
			'facebook'=>$facebook,
			'google'=>$google,
			'json_data'=>$json_e,
			'user_id'=>$this->login_data->id,
			'created_at'=>$this->current_date_time,
		);

		$data_id = $this->Model_manifests->insert_manifests($data_array);

		$path = "files/store_files/$data_id";
		$path_new = "files/store_files/$data_id/pwa-images";

		if(!is_dir($path)) //create the folder if it's not already exists
		{
			mkdir($path,0755,TRUE);
			mkdir($path_new,0755,TRUE);
			mkdir($path_new.'/android',0755,TRUE);
			mkdir($path_new.'/chrome',0755,TRUE);
			mkdir($path_new.'/firefox',0755,TRUE);
			mkdir($path_new.'/msteams',0755,TRUE);
			mkdir($path_new.'/windows',0755,TRUE);
			mkdir($path_new.'/windows10',0755,TRUE);
		}

		$path = "files/store_files/$data_id/";

		$manup = $this->load->view('custom_file/manup',array(), TRUE);
    $fileName=$path."manup.js";
    file_put_contents($fileName, $manup);

		$pwa_custom = $this->load->view('custom_file/pwa-custom',array(), TRUE);
		$pwa_custom = str_replace("{{MANIFESTID}}",$data_id,$pwa_custom);
    $pwa_custom = str_replace("{{USERID}}",$this->login_data->id,$pwa_custom);  
		$fileName=$path."pwa-custom.js";
		file_put_contents($fileName, $pwa_custom);

		$pwa_overlay = $this->load->view('custom_file/pwa-overlay',array(), TRUE);
		$fileName=$path."pwa-overlay.html";
		file_put_contents($fileName, $pwa_overlay);

		$pwa_sw_register = $this->load->view('custom_file/pwabuilder-sw-new',array(), TRUE);
    $fileName=$path."pwabuilder-sw-new.js";
    file_put_contents($fileName, $pwa_sw_register);

    $pwa_sw_register_new = $this->load->view('custom_file/pwabuilder-sw-register-new',array(), TRUE);
    $fileName=$path."pwabuilder-sw-register-new.js";
    file_put_contents($fileName, $pwa_sw_register_new);

    $firebase_messaging_sw = $this->load->view('custom_file/firebase-messaging-sw',array(), TRUE);
    $fileName=$path."firebase-messaging-sw.js";
    file_put_contents($fileName, $firebase_messaging_sw);

    $notification_token = $this->load->view('custom_file/notification_token',array(), TRUE);
    $notification_token = str_replace("{{BASEURL}}",base_url(),$notification_token);
    $notification_token = str_replace("{{PHPTAG}}",'<?php',$notification_token);    
    $fileName=$path."notification_token.php";
    file_put_contents($fileName, $notification_token);

    $instructions = $this->load->view('custom_file/instructions',array(), TRUE);
    $fileName=$path."instructions.txt";
    file_put_contents($fileName, $instructions);

    $manifest = $json_e;
    $fileName=$path."/manifest.json";
    file_put_contents($fileName, $manifest);

    $manup = '';
    $fileName=$path."pwabuilder-offline.html";
    file_put_contents($fileName, $manup);

    if(is_array($_FILES)) {
      $uploadedFile = $_FILES['image']['tmp_name']; 
     	$sourceProperties = getimagesize($uploadedFile);
     	$ext = 'png';
      $imageType = $sourceProperties[2];
      $dirPath = $path_new.'/';
      for ($i=0; $i <count($icon); $i++) { 

				$src = $icon[$i]->src;
				$file_src = $path.$src;
				$sizes =  explode('x', $icon[$i]->sizes);

				switch ($imageType) {
          case IMAGETYPE_PNG:
            $imageSrc = imagecreatefrompng($uploadedFile); 
            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$sizes[0]);
            imagepng($tmp,$file_src);
            break;           

          case IMAGETYPE_JPEG:
            $imageSrc = imagecreatefromjpeg($uploadedFile); 
            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$sizes[0]);
            imagejpeg($tmp,$file_src);
            break;
          
          case IMAGETYPE_GIF:
            $imageSrc = imagecreatefromgif($uploadedFile); 
            $tmp = imageResize($imageSrc,$sourceProperties[0],$sourceProperties[1],$sizes[0]);
            imagegif($tmp,$file_src);
            break;

          default:
            echo "Invalid Image type.";
            exit;
            break;
      	}
			}
      $newFileName = 'image';
      move_uploaded_file($uploadedFile, $dirPath. $newFileName. ".". $ext);
      // echo "Image Resize Successfully.";
    }
    redirect(base_url().'user/apps/setup_manifest/'.$data_id,'refresh');
		// $this->load->view('user/apps/manifest_store',array('id'=>$data_id));
		
	}

	public function setup_manifest($id)
	{
		$this->load->model('Model_manifests');
		$manifest = $this->Model_manifests->get_manifests_details($id);
		$this->load->view('user/apps/setup_manifest',array('id'=>$id,'manifest'=>$manifest));	
	}

	public function setup_manifest_update()
	{
		echo "<pre>";
		// print_r($_POST);
		$file_id = $_POST['file_id'];
		$facebook_content = '';
		$google_content = '';
		$google_bottom = '';
		$google_content1 = '';
		$google_content2 = '';
		$gdpr_content = 'This website uses cookies to ensure you get the best experience on our website.';

		if (isset($_POST['gdpr'])) {
			$gdpr=1;
			$gdpr_content = $_POST['gdpr_content'];
		}
		if (isset($_POST['facebook'])) {
			$facebook=1;
			$facebook_data = $_POST['facebook_content'];
			$facebook_content = $this->load->view('custom_file/facebook',array('facebook_data'=>$facebook_data),true);

		}
		if (isset($_POST['google'])) {
			$google=1;
			$google_data = $_POST['google_content'];
			$google_content1 = $this->load->view('custom_file/google',array('google_data'=>$google_data),true);
			$google_content2 = $this->load->view('custom_file/google_bottom',array('google_data'=>$google_data),true);
		}

		


		
		$file_data = file_get_contents( base_url().'files/store_files/'.$file_id.'/pwa-overlay.html');
		$file_data = str_replace("{{GDPR}}",$gdpr_content,$file_data);
		$file_data = str_replace("{{FACEBOOK}}",$facebook_content,$file_data);
		$file_data = str_replace("{{GOOGLE}}",$google_content1,$file_data);
		$file_data = str_replace("{{GOOGLE_BOTTOM}}",$google_content2,$file_data);

		if (isset($_POST['menu'])) {

			$file_data = str_replace("{{FONTAWESOME}}",'<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">',$file_data);
			$menu=1;
			$icon1 = $_POST['icon1'];
			$murl1 = $_POST['murl1'];

			$icon2 = $_POST['icon2'];
			$murl2 = $_POST['murl2'];


			$icon3 = $_POST['icon3'];
			$murl3 = $_POST['murl3'];

			$icon4 = $_POST['icon4'];
			$murl4 = $_POST['murl4'];

			$menuhtml = '<div class="pab_mobilemenu">';
			
			$pabiconnumber =0;
			if($icon1!="")
			{
				$pabiconnumber = $pabiconnumber+1;
				$menuhtml .= '<a href="'.$murl1.'"><i class="fa '.$icon1.'"></i></a>';
			}
			if($icon2!="")
			{
				$pabiconnumber = $pabiconnumber+1;
				$menuhtml .= '<a href="'.$murl2.'"><i class="fa '.$icon2.'"></i></a>';
			}
			if($icon3!="")
			{
				$pabiconnumber = $pabiconnumber+1;
				$menuhtml .= '<a href="'.$murl3.'"><i class="fa '.$icon3.'"></i></a>';
			}
			if($icon4!="")
			{
				$pabiconnumber = $pabiconnumber+1;
				$menuhtml .= '<a href="'.$murl4.'"><i class="fa '.$icon4.'"></i></a>';
			}
			$anchorwidth = 100 / $pabiconnumber;


			$menucss = '@media(min-width:480px)
			{
				.pab_mobilemenu{display:none;}
			}
			.pab_mobilemenu{
				position:fixed;
				bottom:0;
				right:0;
				left:0;
				background:rgba(0,0,0,0.9);
				z-index:1001;
			}
			.pab_mobilemenu a{
				width: '.$anchorwidth.'%;
				font-size:30px;
				color:#fff;
				text-align:center;
				display:inline-block;
				padding:10px 0;
			}';

			$file_data = str_replace("{{MENUCSS}}",$menucss,$file_data);
			$file_data = str_replace("{{MENUHTML}}",$menuhtml,$file_data);
		}
		else
		{
			$file_data = str_replace("{{FONTAWESOME}}",'',$file_data);
			$file_data = str_replace("{{MENUCSS}}",'',$file_data);
			$file_data = str_replace("{{MENUHTML}}",'',$file_data);
		}

		$path = "files/store_files/$file_id/";
		$fileName=$path."pwa-overlay.html";
		file_put_contents($fileName, $file_data);

		redirect(base_url().'user/apps/download_manifest/'.$file_id,'refresh');

	}

	public function download_manifest($id)
	{
		$this->load->model('Model_manifests');
		$manifest = $this->Model_manifests->get_manifests_details($id);
		$this->load->view('user/apps/manifest_store',array('id'=>$id,'manifest'=>$manifest));	
	}

	public function download_files($id)
	{
		//$id= 180;
		$dir = 'files/store_files/'.$id;

		$archive_name = 'files/store_files/'.$id.".zip"; // name of zip file
		// $archive_folder = 'files/store_files/'.$id; // the folder which you archivate

		Zip($dir,$archive_name);
		 header("Content-Disposition: attachment; filename=".BRAND_NAME."-web-app-kit.zip");
        header("Content-Length: " . filesize($archive_name));
        header("Content-Type: application/octet-stream;");
        readfile($archive_name);
	}

		// print_r($dir);
		// return;
		// header("Content-Disposition: attachment; filename=progressive-web-app-kit.zip");
  //       header("Content-Length: " . filesize($dir.'.zip'));
  //       header("Content-Type: application/octet-stream;");
  //       readfile($dir.".zip");


}

/* End of file Apps.php */
/* Location: ./application/controllers/Apps.php */