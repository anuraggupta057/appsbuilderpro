<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->login_data = $this->session->userdata('login_data');
		$this->login_plan = $this->session->userdata('login_plan');
		if(function_exists('date_default_timezone_set')) {
			date_default_timezone_set("america/new_york");
		}
		if (!$this->login_data->id) {
			redirect('Login');
		}
		if ($this->login_data->is_admin == 1) {
			redirect('superadmin/user');
		}
		$this->current_date_time = date('Y-m-d H:i:s');
	}

	public function index()
	{
		$this->load->model('Model_manifests');
		$data['manifest'] = $this->Model_manifests->get_user_manifests($this->login_data->id);
		$this->load->view('user/notification/view_apps',$data);
	}

	public function create_notification($id)
	{
		$data['id'] = $id;
		$this->load->view('user/notification/create_notification',$data);
	}

	public function send_notification()
	{
		$url_id = $_POST['url_id'];
		$title = $_POST['title'];
		$message = $_POST['message'];
		$notification_url = 'https://appsbuilderpro.s3.amazonaws.com/icons/logo512.png';

		$time = microtime();
		$item_img='';
		$img_name='https://appsbuilderpro.s3.amazonaws.com/icons/logo512.png';
		if($_FILES)
		{       
			if ($_FILES['n_icon']['name']) {
				$info=pathinfo($_FILES['n_icon']['name']);            
				if($info!='')
				{
					$ext=$info['extension'];
					$item_img=$_FILES['n_icon']['name'];
					$target='files/icons/'.$time.$item_img;
					move_uploaded_file($_FILES['n_icon']['tmp_name'],$target);
					$img_name=base_url().'files/icons/'.$time.$item_img;
				}
			}        
		}
		$this->load->model('Model_user_notification');
		$this->load->model('Model_manifests');
		$data_array = array(
			'user_id'=>$this->login_data->id,
			'manifests_id'=>$url_id,
			'title'=>$title,
			'message'=>$message,
			'notification_url'=>$notification_url,
			'icon_url'=>$img_name,
			'create_date_time'=>$this->current_date_time,
		);

		$data_id = $this->Model_user_notification->insert_user_notification($data_array);

		$regIds = $this->Model_manifests->get_manifest_details($url_id);
		echo "<pre>";
		if ($regIds) {
			foreach ($regIds as $key) {
				$payload = array(
					'body'   => $message,
		      'title'   => $title,
		      'click_action'   => $notification_url,
		      'vibrate' => 1,
		      'sound'   => 1,
		      'icon' => $img_name
				);	
				// print_r($payload);
				// echo "<br>";
				send_notification($payload,$title,$message,$img_name,$key->token);	
			}
		}
		$this->session->set_flashdata('Message','Notification Send Successfully!');
		redirect(base_url('user/notification'),'refresh');

	}

}

/* End of file Notification.php */
/* Location: ./application/controllers/Notification.php */