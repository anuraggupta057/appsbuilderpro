<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->login_data = $this->session->userdata('login_data');
		$this->login_plan = $this->session->userdata('login_plan');
		if(function_exists('date_default_timezone_set')) {
		    date_default_timezone_set("america/new_york");
		}
		if (!$this->login_data->id) {
			redirect('Login');
		}
		if ($this->login_data->is_admin == 1) {
			redirect('superadmin/user');
		}
		$this->current_date_time = date('Y-m-d H:i:s');
	}

	public function view_user()
	{
		if (in_array("3", $this->login_plan)) { 
	    $this->load->model('Model_login');
			$data['users'] = $this->Model_login->get_resaller_user($this->login_data->id); 
			$this->load->view('user/user/view_user', $data);
    }
	}

	public function add_user()
	{
		if (in_array("3", $this->login_plan)) { 
	   	$this->load->view('user/user/add_user');
    }
	}

	public function save_user()
	{
		$name = $_POST['name'];
		$email = $_POST['email'];
		$password = md5($_POST['password']);

		$this->load->model('Model_login');

		$check_email = $this->Model_login->check_count_email($email);
		if ($check_email == 0) {
			$data_array = array(
				'name'=>$name,
				'email'=>$email,
				'is_reseller'=>$this->login_data->id,
				'password'=>md5($password),
				'created_at'=>date('Y-m-d H:i:s'),
			);
			$login_id = $this->Model_login->insert_login($data_array);
			$login_data = $this->Model_login->get_login_by_id_md5(md5($login_id));

			if ($login_data) 
			{
				$this->session->set_flashdata('Message','User Added Success!');
				echo "Valid";
			}
			else
			{
				echo "Invalid";
			}
		}
		else
		{
			echo "Email Already";
		}
	}

	public function change_user_status()
	{
		$user_id = $_POST['user_id'];
		$this->load->model('Model_login');
		$user = $this->Model_login->get_login_by_id_md5(md5($user_id));
		if ($user->is_status == 0) {
			$data_array = array(
				'is_status'=>1,
			);

			$data_id = $this->Model_login->update_login($data_array,$user_id);
			echo "1";
		}
		if ($user->is_status == 1) {
			$data_array = array(
				'is_status'=>0,
			);

			$data_id = $this->Model_login->update_login($data_array,$user_id);
			echo "0";
		}
	}

	public function modal_update_user_password_by_admin()
	{
		$user_id = $_POST['user_id'];
		$this->load->model('Model_login');
		$user = $this->Model_login->get_login_by_id_md5(md5($user_id));
		$this->load->view('user/user/modal_change_password',array('user'=>$user));
	}

	public function update_user_password_by_admin()
	{
		$password = $_POST['password'];
		$user_id = $_POST['user_id'];
		$this->load->model('Model_login');
		$data_array = array(
			'password'=>md5($password),
		);

		$data_id = $this->Model_login->update_login($data_array,$user_id);
		if ($data_id) {
			echo "Valid";
		}
	}

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */