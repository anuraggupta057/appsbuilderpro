<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function register()
	{
		$this->load->view('sign_up');
	}

	public function check_login()
	{
		$email = $_POST['email'];
		$password = md5($_POST['password']);

		$this->load->model('Model_login');
		$login_data = $this->Model_login->login_user($email,$password);

		if ($login_data) 
		{
			$login_plan = $login_data->plan_type;
			$login_plan = explode(',',$login_plan);
			$this->session->set_userdata('login_data',$login_data);
			$this->session->set_userdata('login_plan',$login_plan);
			$data_array_log = array(
				'user_id'=>$login_data->id,
				'type'=>0,
				'created_date_time'=>date('Y-m-d H:i:s'),
			);

			$this->load->model('Model_user_log');
			$data_log_id = $this->Model_user_log->insert_user_log($data_array_log);

			echo "Valid";
			$this->session->set_flashdata('Message','Login Success!');
		}
		else
		{
			echo "Invalid";
		}
	}

	public function register_user()
	{
		$name = $_POST['name'];
		$email = $_POST['email'];
		$password = $_POST['password'];

		$this->load->model('Model_login');

		$check_email = $this->Model_login->check_count_email($email);
		if ($check_email == 0) {
			$data_array = array(
				'name'=>$name,
				'email'=>$email,
				'password'=>md5($password),
				'created_at'=>date('Y-m-d H:i:s'),
			);
			$login_id = $this->Model_login->insert_login($data_array);
			$login_data = $this->Model_login->get_login_by_id_md5(md5($login_id));

			if ($login_data) 
			{
				$this->session->set_userdata('login_data',$login_data);
				$data_array_log = array(
					'user_id'=>$login_data->id,
					'type'=>0,
					'created_date_time'=>date('Y-m-d H:i:s'),
				);

				$this->load->model('Model_user_log');
				$data_log_id = $this->Model_user_log->insert_user_log($data_array_log);
				echo "Valid";
				$this->session->set_flashdata('Message','Sign Up Success!');
			}
			else
			{
				echo "Invalid";
			}
		}
		else
		{
			echo "Email Already";
		}

		
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url(),'refresh');
	}

	public function forgot()
	{
		$this->load->view('forgot');
	}

	public function forgot_password()
	{
		$email = $_POST['email'];
		$this->load->model('Model_login');
		$login_data = $this->Model_login->check_email_address($email);
		if($login_data)
		{
			$message = $this->load->view('email/forgot_password',array('login_data'=>$login_data),true);
			if (sendEmail('Forgot Password',$message,$login_data->email)) {
				echo "Valid";
			}
		}
		else
		{
			echo "Invalid";
		}
		// print_r(sendEmail('TEsting','Message','anuraggupta057@gmail.com'));
	}

	public function recover_password()
	{
		$email = $_GET['email'];
		$uid = $_GET['uid'];

		$this->load->model('Model_login');
		$check_user = $this->Model_login->check_user_and_email($email,$uid);
		if ($check_user) {
			$this->load->view('recover_password', array('check_user'=>$check_user));
		}
		else
		{
			echo "Not Page Found";
		}
		// print_r($check_user);
	}

	public function password_recover()
	{
		$user_id = $_POST['user_id'];
		$password = $_POST['password'];

		$this->load->model('Model_login');
		$data_array = array(
			'password'=>md5($password),
		);

		$data_id = $this->Model_login->update_login_by_md5($data_array,$user_id);
		if ($data_id) {
			$this->session->set_flashdata('Message','Password Change Successfully Please Login');
			echo "Valid";
		}
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */