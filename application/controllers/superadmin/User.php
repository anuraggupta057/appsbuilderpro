<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->login_data = $this->session->userdata('login_data');
		$this->login_role = $this->session->userdata('login_role');
		if(function_exists('date_default_timezone_set')) {
			date_default_timezone_set("america/new_york");
		}
		if (!$this->login_data->id) {
			redirect('Login');
		}
		if ($this->login_data->is_admin == 0) {
			redirect('user/dashboard');
		}
		$this->current_date_time = date('Y-m-d H:i:s');
	}

	public function index()
	{
		$this->load->model('Model_login');
		$data['user'] = $this->Model_login->get_all_users();
		$this->load->view('superadmin/view_user',$data);
	}

	public function login_as()
	{
		$this->session->set_userdata('login_data_superadmin',$this->login_data);
		$this->load->model('Model_login');
		$login_user = $this->Model_login->get_login_by_id_md5(md5($_GET['user_id']));
		// print_r($login_user)
		$this->session->set_userdata('login_data',$login_user);
		redirect(base_url().'user/dashboard','refresh');
	}

	public function change_user_status()
	{
		$user_id = $_POST['user_id'];
		$this->load->model('Model_login');
		$user = $this->Model_login->get_login_by_id_md5(md5($user_id));
		if ($user->is_status == 0) {
			$data_array = array(
				'is_status'=>1,
			);

			$data_id = $this->Model_login->update_login($data_array,$user_id);
			echo "1";
		}
		if ($user->is_status == 1) {
			$data_array = array(
				'is_status'=>0,
			);

			$data_id = $this->Model_login->update_login($data_array,$user_id);
			echo "0";
		}
	}

	public function modal_update_user_password_by_admin()
	{
		$user_id = $_POST['user_id'];
		$this->load->model('Model_login');
		$user = $this->Model_login->get_login_by_id_md5(md5($user_id));
		$this->load->view('superadmin/modal_change_password',array('user'=>$user));
	}

	public function update_user_password_by_admin()
	{
		$password = $_POST['password'];
		$user_id = $_POST['user_id'];
		$this->load->model('Model_login');
		$data_array = array(
			'password'=>md5($password),
		);

		$data_id = $this->Model_login->update_login($data_array,$user_id);
		if ($data_id) {
			echo "Valid";
		}
	}

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */