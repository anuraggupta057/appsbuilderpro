<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification_token extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if (isset($_GET['token'])) {
			$token = $_GET['token'];
			$url = $_GET['url'];
			$manifestId = $_GET['manifestId'];
			$userId = $_GET['userId'];
			$this->load->model('Model_notification_tokens');

			$url = rtrim($url, '/');

			$check_token = $this->Model_notification_tokens->check_token_details($token,$url);
			if (!$check_token) {
				$data_token = array(
					'token'=>$token,
					'domain'=>$url,
					'user_id'=>$userId,
					'manifest_id'=>$manifestId,
				);
				$data_id = $this->Model_notification_tokens->insert_token($data_token);
				if ($data_id) {
					echo "Success";
				}
			}
			else
			{
				echo "Already Token Added";
			}
		}
		
	}

}

/* End of file Notification_token.php */
/* Location: ./application/controllers/Notification_token.php */