<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_manifests extends CI_Model {

	public $table = 'manifests';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function insert_manifests($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function get_manifests_details($id)
	{
		$sql = "SELECT * FROM `manifests` WHERE id = '$id' ORDER BY id DESC";
		$res = $this->db->query($sql);
		return $res->row();
	}

	public function get_user_manifests($user_id)
	{
		$sql = "SELECT * FROM `manifests` WHERE user_id = '$user_id' ORDER BY id DESC";
		$res = $this->db->query($sql);
		return $res->result();
	}

	public function get_manifest_details($id)
	{
		$sql = "SELECT NT.token FROM `notification_tokens` NT , manifests M WHERE M.id = '$id' AND M.url = NT.domain";
		$res = $this->db->query($sql);
		return $res->result();
	}

	public function count_userapp($user_id)
	{
		$sql = "SELECT count(id) as count FROM `manifests` WHERE user_id = '$user_id' ORDER BY id DESC";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}

}

/* End of file Model_manifests.php */
/* Location: ./application/models/Model_manifests.php */