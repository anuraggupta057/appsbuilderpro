<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_notification_tokens extends CI_Model {

	public $table = 'notification_tokens';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function insert_token($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}
	public function check_token_details($token,$domain)
	{
		$sql = "SELECT * FROM $this->table WHERE token = '$token' AND domain = '$domain'";
		$res = $this->db->query($sql);
		return $res->row();
	}

	public function count_user_subscriber($user_id)
	{
		$sql = "SELECT count(id) as count FROM $this->table WHERE user_id = '$user_id' ORDER BY id DESC";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}

}

/* End of file Model_notification_tokens.php */
/* Location: ./application/models/Model_notification_tokens.php */