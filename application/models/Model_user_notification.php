<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_user_notification extends CI_Model {

	public $table = 'user_notification';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();	
	}

	public function insert_user_notification($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function count_user_notification($user_id)
	{
		$sql = "SELECT count(id) as count FROM `user_notification` WHERE user_id = '$user_id' ORDER BY id DESC";
		$res = $this->db->query($sql);
		return $res->row()->count;
	}

}

/* End of file Model_user_notification.php */
/* Location: ./application/models/Model_user_notification.php */