<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_login extends CI_Model {

	public $table = 'login';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function login_user($email,$password)
	{
		$sql = "SELECT * FROM $this->table WHERE `email` = '$email' and password = '$password' ";
		$query = $this->db->query($sql);
		return $query->row();
	}

    public function get_all_users()
    {
        $sql = "SELECT * FROM $this->table WHERE is_admin = 0";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_resaller_user($id)
    {
        $sql = "SELECT * FROM $this->table WHERE is_admin = 0 and is_reseller = '$id'";
        $query = $this->db->query($sql);
        return $query->result();
    }

	public function insert_login($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update_login($data,$id)
    {
    	$this->db->where('id', $id);
    	$this->db->update($this->table, $data);
    	return $id;
    }

    public function update_login_by_md5($data,$id)
    {
        $this->db->where('md5(id)', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    public function delete_login($id)
    {
    	$this->db->where('id', $id);
    	$this->db->delete($this->table);
    	return $id;
    }

    public function get_login_by_id_md5($id)
    {
        $sql = "SELECT * FROM $this->table WHERE md5(id) = '$id' ";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function check_email_address($email)
    {
        $sql = "SELECT * FROM  login L WHERE L.email = '$email'";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function check_count_email($email)
    {
        $sql = "SELECT count(L.id) as count  FROM  login L WHERE L.email = '$email'";
        $query = $this->db->query($sql);
        return $query->row()->count;
    }

    public function check_user_and_email($email,$id)
    {
        $sql = "SELECT * FROM  login L WHERE L.email = '$email' AND md5(L.id) = '$id'";
        $query = $this->db->query($sql);
        return $query->row();
    }

}

/* End of file Model_login.php */
/* Location: ./application/models/Model_login.php */