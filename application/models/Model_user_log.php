<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_user_log extends CI_Model {

	public $table = 'user_log';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function insert_user_log($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}


}

/* End of file Model_user_log.php */
/* Location: ./application/models/Model_user_log.php */