<!doctype html>
<html lang="en">
	<head>
		<?php $this->load->view('bars/head');?>
	</head>
	<body>

		<div class="o-page">
			<div class="o-page__sidebar js-page-sidebar">
				<?php $this->load->view('bars/sidebar');?>
			</div>

			<main class="o-page__content">
				<?php $this->load->view('bars/header');?>

				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="c-card c-card--center">
								<div class="table-responsive">
									<table id="table_dt" class="display" style="width:100%">
										<thead>
											<th>Sr.No.</th>
											<th>Name</th>
											<th>Email</th>
											<th>Password</th>
											<th>Plan</th>
											<th>Log</th>
											<th>Active</th>
											<th>Login</th>
										</thead>
										<tbody>
											<?php $x =1; foreach ($user as $key){ ?>
											<tr>
												<td><?php echo $x++; ?></td>
												<td><?php echo $key->name; ?></td>
												<td><?php echo $key->email; ?></td>
												<td><button class="c-btn c-btn--small" onclick="modal_update_user_password_by_admin('<?php echo $key->id;?>');">Update</button></td>
												<td><button class="c-btn c-btn--small">Plan </button></td>
												<td><button class="c-btn c-btn--small">Log </button></td>
												<td>
													<?php if($key->is_status == 0){ ?>
													<button class="c-btn c-btn--small" onclick="change_user_status('<?php echo $key->id; ?>',this);">Active</button>
													<?php } else if($key->is_status == 1){ ?>
													<button class="c-btn c-btn--warning c-btn--small" onclick="change_user_status('<?php echo $key->id; ?>',this);">Inactive</button>
													<?php }?>
												</td>
												<td><a class="c-btn c-btn--small" href="<?php echo base_url();?>superadmin/user/login_as?user_id=<?php echo $key->id;?>">Login As</a></td>
											</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<!-- Modal -->
          <div class="c-modal modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
            
          </div><!-- // .c-modal -->
					<?php $this->load->view('bars/footer');?>
				</div>
			</main>
		</div>

		<!-- Main JavaScript -->
		<?php $this->load->view('bars/js');?>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
			    $('#table_dt').DataTable();
			} );

			function change_user_status(user_id,button)
			{
				$.ajax({
					url: base_url + "superadmin/user/change_user_status",
					type: "POST",
					data: { user_id: user_id },
					success: function (result) {
						console.log(result);
						if (result == "1") {
							$(button).html('Inactive');
							$(button).addClass('c-btn--warning');
							toastr.options = {
							  "closeButton": false,
							  "debug": false,
							  "newestOnTop": false,
							  "progressBar": true,
							  "preventDuplicates": true,
							  "onclick": null,
							  "showDuration": "100",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "show",
							  "hideMethod": "hide",
							  "positionClass": "toast-bottom-right",
							};
							toastr.error("User Inactive");
						} else {
							$(button).html('Active');
							$(button).removeClass('c-btn--warning');
							toastr.options = {
							  "closeButton": false,
							  "debug": false,
							  "newestOnTop": false,
							  "progressBar": true,
							  "preventDuplicates": true,
							  "onclick": null,
							  "showDuration": "100",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "show",
							  "hideMethod": "hide",
							  "positionClass": "toast-bottom-right",
							};
							toastr.success("User Active");
						}
					},
				});
			}

			function modal_update_user_password_by_admin(user_id)
			{
				$.ajax({
					url: base_url + "superadmin/user/modal_update_user_password_by_admin",
					type: "POST",
					data: { user_id: user_id },
					success: function (result) {
						$('#modal2').html(result);
						$('#modal2').modal('show');
					},	
				});
			}

			function update_user_password_by_admin()
			{
				password = $('#password').val();
				user_id = $('#user_id').val();

				$.ajax({
					url: base_url + "superadmin/user/update_user_password_by_admin",
					type: "POST",
					data: { user_id: user_id,password:password },
					success: function (result) {
						if (result == 'Valid') 
						{
							toastr.options = {
							  "closeButton": false,
							  "debug": false,
							  "newestOnTop": false,
							  "progressBar": true,
							  "preventDuplicates": true,
							  "onclick": null,
							  "showDuration": "100",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "show",
							  "hideMethod": "hide",
							  "positionClass": "toast-bottom-right",
							};
							toastr.success("Password Updated successfully");
							$('#modal2').modal('hide');
						}
					},	
				});
			}
		</script>
	</body>
</html>