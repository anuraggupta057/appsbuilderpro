<!doctype html>
<html lang="en">
  <head>
    <?php $this->load->view('bars/head');?>
  </head>
  <body>

    <div class="o-page o-page--center">
      <div class="o-page__card">
        <div class="c-card c-card--center">
          <span class="c-icon c-icon--large u-mb-small">
            <img src="<?php echo base_url();?>assets/img/jv.png" alt="Neat">
          </span>

          <h4 class="u-mb-medium">Welcome Back :)</h4>
          <form id="login_form">
            <div class="c-field">
              <label class="c-field__label">Email Address</label>
              <input class="c-input u-mb-small" type="email" placeholder="e.g. adam@sandler.com" id="email" required>
            </div>
            <div class="c-field">
              <label class="c-field__label">Password</label>
              <input class="c-input u-mb-small" type="password" placeholder="Numbers, Pharagraphs Only" id="password" required>
            </div>
            <div class="c-field">
              <a href="<?php echo base_url(); ?>login/forgot">Forgot Password?</a>
              <br>
              <br>
            </div>
            <button class="c-btn c-btn--fullwidth c-btn--info">Login</button>
          </form>
        </div>
      </div>
    </div>

    <!-- Main JavaScript -->
    <script src="<?php echo base_url();?>assets/js/neat.min.js?v=1.0"></script>
    <script src="<?php echo base_url();?>assets/js/login.js"></script>
    <script src="<?php echo base_url();?>assets/toastr/toastr.js"></script>
    <script type="text/javascript">
      $( "#login_form" ).submit(function( event ) {
        login_check();
        event.preventDefault();
      });
      <?php if($this->session->flashdata('Message')) { ?>
          toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "100",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "show",
            "hideMethod": "hide",
            "positionClass": "toast-bottom-right",
          };
          toastr.success("<?php echo $this->session->flashdata('Message'); ?>");
        <?php };?>
    </script>
  </body>
</html>