<div class="row">
  <div class="col-12">
    <footer class="c-footer">
      <p>© <?php echo date('Y');?> - <?php echo date('Y')+1;?> <?php echo BRAND_NAME;?></p>
      <span class="c-footer__divider">|</span>
      <nav>
        <a class="c-footer__link" href="https://appsbuilderpro.io/tos.html" target="_blank">Terms of Service</a>
        <a class="c-footer__link" href="https://appsbuilderpro.io/privacy.html" target="_blank">Privacy</a>
        <a class="c-footer__link" href="https://appsbuilderpro.io/gdpr.html" target="_blank">GDPR</a>
        <a class="c-footer__link" href="https://appsbuilderpro.io/support.html" target="_blank">Support</a>
        <a class="c-footer__link" href="https://appsbuilderpro.io/disclaimer.html" target="_blank">Disclaimer</a>
      </nav>
    </footer>
  </div>
</div>