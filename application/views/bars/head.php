<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title><?= BRAND_NAME; ?></title>
<meta name="description" content="<?= BRAND_NAME; ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Google Font -->
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

<!-- Favicon -->
<link rel="apple-touch-icon" href="apple-touch-icon.png">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<script src="https://kit.fontawesome.com/6203886820.js" crossorigin="anonymous"></script>

<!-- Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/neat.min.css?v=1.0">
<!-- <link rel="stylesheet" href="<?php //echo base_url();?>assets/css/custom.css"> -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
<link href="<?php echo base_url();?>assets/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
<base href="<?php echo base_url(); ?>">

<style type="text/css">
	table.dataTable tbody tr{
		border: none;
	}
	table.dataTable.stripe tbody tr.odd, table.dataTable.display tbody tr.odd{
		background: transparent;
	}
	table.dataTable.display tbody tr.odd>.sorting_1, table.dataTable.order-column.stripe tbody tr.odd>.sorting_1{
		background: transparent;
	}
	table.dataTable.display tbody tr.even>.sorting_1, table.dataTable.order-column.stripe tbody tr.even>.sorting_1{
		background: transparent;
	}
	table.dataTable tbody th, table.dataTable tbody td{
		padding: 15px 10px;
	}
	.dataTables_wrapper .dataTables_paginate .paginate_button:hover{
		background: #768093;
		color: #fff;
	}
	.dataTables_wrapper .dataTables_paginate .paginate_button:hover a{
		color: #fff;
	}
	 #password-strength-status {
                padding: 5px 10px;
                color: #FFFFFF;
                border-radius: 4px;
                margin-top: 5px;
                font-size: 12px;
            }

            .medium-password {
                background-color: #b7d60a;
                border: #BBB418 1px solid;
            }

            .weak-password {
                background-color: #ce1d14;
                border: #AA4502 1px solid;
            }

            .strong-password {
                background-color: #12CC1A;
                border: #0FA015 1px solid;
            }
</style>