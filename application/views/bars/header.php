<?php $login_data =  $this->session->userdata('login_data'); ?>
<header class="c-navbar u-mb-medium">
  <button class="c-sidebar-toggle js-sidebar-toggle">
    <i class="feather icon-align-left"></i>
  </button>

  <h2 class="c-navbar__title">Welcome to AppsBuilderPro 
    <?php if ($this->session->userdata('login_data_superadmin')){ ?>
    <a href="<?php echo base_url(); ?>user/dashboard/superadmin" class="c-btn c-btn--small c-btn--primary" style="color:#fff;">Superadmin</a>  
    <?php } ?>
  </h2>
  <div class="c-dropdown dropdown">
    <div class="c-avatar c-avatar--xsmall dropdown-toggle" id="dropdownMenuAvatar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">
      <?php 
      $img = base_url().'assets/img/user.png';
      if ($login_data->img != ''){ 
         $img = base_url().'files/user_img/'.$login_data->img;
      }?>
      <img class="c-avatar__img" src="<?php echo $img; ?>" alt="Adam Sandler">
    </div>

    <div class="c-dropdown__menu has-arrow dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuAvatar">
      <a class="c-dropdown__item dropdown-item" href="<?php echo base_url(); ?>user/profile/edit_profile">Edit Profile</a>
      <a class="c-dropdown__item dropdown-item" href="<?php echo base_url(); ?>user/profile/edit_profile?password_change">Change Password</a>
      <a class="c-dropdown__item dropdown-item" href="<?php echo base_url(); ?>login/logout">Log out</a>
    </div>
  </div>
</header>