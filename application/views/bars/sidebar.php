<?php $login_data = $this->session->userdata('login_data'); ?>
<?php $login_plan = $this->session->userdata('login_plan'); ?>

<aside class="c-sidebar">
  <div class="c-sidebar__brand">
    <a href="javascript:void(0);"><img src="<?php echo base_url();?>assets/img/logo.png" alt="Neat"></a>
  </div>

  <!-- Scrollable -->
  <div class="c-sidebar__body">
    <?php if ($login_data->is_admin == 0){ ?>
    <ul class="c-sidebar__list">
      <li>
        <a class="c-sidebar__link" href="<?= base_url(); ?>user/dashboard">
          <i class="c-sidebar__icon feather icon-home"></i>Dashboard
        </a>
      </li>
    </ul>
    <span class="c-sidebar__title">Apps</span>
    <ul>
      <li>
        <a class="c-sidebar__link" href="<?= base_url(); ?>user/apps">
          <i class="c-sidebar__icon feather  icon-list"></i>My Apps
        </a>
      </li>
      <li>
        <a class="c-sidebar__link" href="<?= base_url(); ?>user/apps/create_app">
          <i class="c-sidebar__icon feather  icon-tablet"></i>Create App
        </a>
      </li>
    </ul>
    <span class="c-sidebar__title">Notification</span>
    <ul>
      <li>
        <a class="c-sidebar__link" href="<?= base_url(); ?>user/notification">
          <i class="c-sidebar__icon feather  icon-bell"></i>Push Notification
        </a>
      </li>
    </ul>

    <span class="c-sidebar__title">Other</span>
    <ul>
      <li>
        <a class="c-sidebar__link" href="<?php echo base_url(); ?>user/pages/support">
          <i class="c-sidebar__icon fas fa-ticket-alt"></i>Support
        </a>
      </li>
      <li>
        <a class="c-sidebar__link" href="<?php echo base_url(); ?>user/pages/bonus">
          <i class="fas fa-gift c-sidebar__icon"></i>Bonus
        </a>
      </li>
      <li>
        <a class="c-sidebar__link" href="<?php echo base_url(); ?>user/pages/wordpress">
          <i class="c-sidebar__icon fab fa-wordpress"></i>Wordpress Plugin
        </a>
      </li>
      <li>
        <a class="c-sidebar__link" href="<?php echo base_url(); ?>user/pages/tutorial">
          <i class="fas fa-graduation-cap c-sidebar__icon"></i>Tutorial
        </a>
      </li>
    </ul>  
    <?php if (in_array("3", $login_plan)) { ?>
    <span class="c-sidebar__title">Users</span>
    <ul>
      <li>
        <a class="c-sidebar__link" href="<?php echo base_url(); ?>user/user/view_user">
          <i class="c-sidebar__icon feather icon-user"></i>Users
        </a>
      </li>
    </ul> 
    <?php }?>
    
    <?php } ?>
    <?php if ($login_data->is_admin == 1){ ?>
    <span class="c-sidebar__title">User</span>
    <ul>
      <li>
        <a class="c-sidebar__link" href="<?php echo base_url(); ?>superadmin/user">
          <i class="c-sidebar__icon feather  icon-user"></i>View User
        </a>
      </li>
    </ul>  
    <?php }?>
  </div>
  

  <a class="c-sidebar__footer" href="<?php echo base_url(); ?>login/logout">
    Logout <i class="c-sidebar__footer-icon feather icon-power"></i>
  </a>
</aside>