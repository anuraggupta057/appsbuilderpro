importScripts("https://www.gstatic.com/firebasejs/7.16.1/firebase-app.js");
importScripts(
    "https://www.gstatic.com/firebasejs/7.16.1/firebase-messaging.js",
);
// For an optimal experience using Cloud Messaging, also add the Firebase SDK for Analytics.
importScripts(
    "https://www.gstatic.com/firebasejs/7.16.1/firebase-analytics.js",
);

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.

var firebaseConfig = {
    apiKey: "<?php echo APIKEY; ?>",
    authDomain: "<?php echo AUTHDOMAIN; ?>",
    databaseURL: "<?php echo DATABASEURL; ?>",
    projectId: "<?php echo PROJECTID; ?>",
    storageBucket: "<?php echo STORAGEBUCKET; ?>",
    messagingSenderId: "<?php echo MESSAGINGSENDERID; ?>",
    appId: "<?php echo APPID; ?>",
    measurementId: "<?php echo MEASUREMENTID; ?>"
};

firebase.initializeApp(firebaseConfig);
// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
    console.log(payload.data);
    // Customize notification here
    json_data = payload.data.notification;
    json_data = JSON.parse(json_data);
    console.log(json_data);
    const notificationTitle = json_data.title;
    const notificationOptions = {
        body: json_data.body,
        icon: json_data.icon,
        click_action: "https://google.com",
    };
    
    self.addEventListener('notificationclick', function(event) {
      event.notification.close();
    
      //window.open(json_data.payload.click_action,'_blank');
    });

    return self.registration.showNotification(
        notificationTitle,
        notificationOptions,
    );
});