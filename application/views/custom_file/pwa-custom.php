
if (typeof(jQuery) == 'undefined') {
    //jquery not present on website
   (function() {
       // Load jquery script if doesn't exist
       var script = document.createElement("SCRIPT");
       script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js';
       script.type = 'text/javascript';
       script.onload = function() {
           pwaBuilderOverlayInit();
       };
       document.getElementsByTagName("head")[0].appendChild(script);
   })();
}else{
    //jquery present on website
   pwaBuilderOverlayInit();
}

function pwaBuilderOverlayInit(){
$(document).one("ready",function(){
    $.ajax({
        url: 'pwa-overlay.html',
        success: function(response){
            $("body").append(response);
        }
    });

   });
}
        // Initialize Firebase
        // TODO: Replace with your project's customized code snippet
  var config = {
    apiKey: "<?php echo APIKEY; ?>",
    authDomain: "<?php echo AUTHDOMAIN; ?>",
    databaseURL: "<?php echo DATABASEURL; ?>",
    projectId: "<?php echo PROJECTID; ?>",
    storageBucket: "<?php echo STORAGEBUCKET; ?>",
    messagingSenderId: "<?php echo MESSAGINGSENDERID; ?>",
    appId: "<?php echo APPID; ?>",
    measurementId: "<?php echo MEASUREMENTID; ?>"
  };
  firebase.initializeApp(config);

        const messaging = firebase.messaging();
        messaging
            .requestPermission()
            .then(function () {
                console.log("Notification permission granted.");

                // get the token in the form of promise
                return messaging.getToken()
            })
            .then(function(token) {
                  var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                      
                    }
                };
                xhttp.open("GET", "notification_token.php?token="+token+"&url="+window.location.href+"&manifestId={{MANIFESTID}}&userId={{USERID}}", true);
                xhttp.send();
            })
            .catch(function (err) {
                console.log("Unable to get permission to notify.", err);
            });
    
  



