<!doctype html>
<html lang="en">
	<head>
		<?php $this->load->view('bars/head');?>
		
	</head>
	<body>

		<div class="o-page">
			<div class="o-page__sidebar js-page-sidebar">
				<?php $this->load->view('bars/sidebar');?>
			</div>

			<main class="o-page__content">
				<?php $this->load->view('bars/header');?>

				<div class="container">
					<div class="row">
						<div class="col-12">
							<?php if($this->session->flashdata('Message')) { ?>
							<div class="c-alert c-alert--success alert u-mb-medium">
		            <span class="c-alert__icon">
		              <i class="feather icon-check"></i>
		            </span>

		            <div class="c-alert__content">
		              <h4 class="c-alert__title"><?php echo $this->session->flashdata('Message'); ?></h4>
		            </div>

		            <button class="c-close" data-dismiss="alert" type="button">×</button>
		          </div>
							<?php };?>
							<div class="c-card c-card--center">
								<div class="c-table-responsive@wide">
									<table id="table_dt" class="display c-table" style="width:100%">
										<thead class="c-table__head">
											<tr class="c-table__row">
												<th class="c-table__cell c-table__cell--head">Sr.No.</th>
												<th class="c-table__cell c-table__cell--head">Url</th>
												<th class="c-table__cell c-table__cell--head">Notification</th>
											</tr>
											
										</thead>
										<tbody>
											<?php $x=1; foreach ($manifest as $key ) { ?>
											<tr class="c-table__row">
												<td class="c-table__cell"><?php echo $x++; ?></td>
												<td class="c-table__cell"><?php echo $key->url; ?></td>
												<td class="c-table__cell"><a href="<?php echo base_url(); ?>user/notification/create_notification/<?php echo $key->id;?>">Send</a></td>
											</tr>
											<?php } ?>
											
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					
					<?php $this->load->view('bars/footer');?>
				</div>
			</main>
		</div>

		<!-- Main JavaScript -->
		<?php $this->load->view('bars/js');?>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
			    $('#table_dt').DataTable();
			} );
		</script>
	</body>
</html>