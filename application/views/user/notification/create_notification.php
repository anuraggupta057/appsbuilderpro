<?php $login_plan = $this->session->userdata('login_plan'); ?>

<!doctype html>
<html lang="en">
	<head>
		<?php $this->load->view('bars/head');?>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
		<style type="text/css">
			.modal .c-pipeline__card{
				box-shadow: none;
				border: 1px solid rgba(0,0,0,0.2);
			}
		</style>
	</head>
	<body>

		<div class="o-page">
			<div class="o-page__sidebar js-page-sidebar">
				<?php $this->load->view('bars/sidebar');?>
			</div>

			<main class="o-page__content">
				<?php $this->load->view('bars/header');?>

				<div class="container">
					<div class="o-page">
						<div class="o-page__card">
							<div class="c-card c-card--center">
								<span class="c-icon c-icon--large u-mb-small">
									<img class="ic-size" src="<?php echo base_url(); ?>assets/img/jv.png">
								</span>
								<h4 class="u-mb-medium">Enter Notification Details</h4>
									<form role="form" method="POST" action="<?php echo base_url(); ?>user/notification/send_notification" enctype="multipart/form-data">
										<input type="hidden" name="url_id" value="<?php echo $id; ?>">
										<div class="c-field form-group">
											<label for="title" class="c-field__label control-label">Title</label>
											<input type="text" id="title" class="c-input u-mb-small" name="title" value="" required="">
										</div>
										<div class="c-field form-group">
											<label for="message" class="c-field__label control-label">Message</label>
											<textarea id="message" class="c-input u-mb-small" type="text" value=" " name="message" required=""></textarea>
										</div>
										<div class="c-field form-group" style="display: none;">
											<label for="notification_url" class="c-field__label control-label">Url</label>
											<input type="text" id="notification_url" class="c-input u-mb-small" name="notification_url">
										</div>
										<div class="c-field">
										  <label for="n_icon" class="c-field__label control-label">Icon url</label>
										  <input id="n_icon" class="c-input u-mb-small" type="file" name="n_icon" onchange="show_item_img('n_icon','output_image')">
										</div>
										<div class="c-field row">
						          	<div class="col-lg-8">
						            	<label class="c-field__label control-label">Preview</label>
						          	</div>
						          	<div class="col-lg-8">
						            	<img id="output_image" src="" alt="" width="100" height="100">
						          	</div>
						        </div>
						        <?php 
						        if (in_array("2", $login_plan)) { ?>
								   	<div class="c-field">
											<button type="button" class="c-btn c-btn--fullwidth c-btn--outline" data-toggle="modal" data-target="#modal2" style="margin-bottom:10px; float:left;">Choose from template</button>
										</div>
								    <?php } ?>
										
									<button type="submit" class="c-btn c-btn--fullwidth c-btn--info">SEND</button>
								</form>
							</div>
						</div>
					</div>
					<?php $this->load->view('bars/footer');?>
				</div>
			</main>
		</div>
		<div class="c-modal modal fade c-modal--huge" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2">
      <div class="c-modal__dialog modal-dialog " role="document">
        <div class="c-modal__content">
          <div class="c-modal__body">
            <span class="c-modal__close" data-dismiss="modal" aria-label="Close">
              <i class="feather icon-x"></i>
            </span>
            <h3 class="u-mb-small">Select Template</h3>
            <div class="set_box" style="max-height: 60vh; overflow-y: scroll; overflow-x: hidden;">
            	<div class="row">
	            	<div class="col-sm-6">
	            		<div class="c-pipeline" >
			              <div class="c-pipeline__card" id="box1">
			                <div class="o-media" style="text-align: left;">
			                  <div class="o-media__body">
			                    <h5 class="c-pipeline__card-title">[XXX] Membership unlocked!</h5>
			                    <p class="c-pipeline__card-subtitle">You are one of the Luck Susbcribers. Claim Access Now.</p>
			                  </div>
			                </div>
			              </div>
			            </div>
	            	</div>
	            	<div class="col-sm-6">
	            		<div class="c-pipeline" >
			              <div class="c-pipeline__card" id="box2">
			                <div class="o-media" style="text-align: left;">
			                  <div class="o-media__body" >
			                    <h5 class="c-pipeline__card-title">[XX%] Off on [Brand Name]</h5>
			                    <p class="c-pipeline__card-subtitle">Offer Valid Till [XX/Minutes/Hours/Midnight]</p>
			                  </div>
			                </div>
			              </div>
			            </div>
	            	</div>
	            	<div class="col-sm-6">
	            		<div class="c-pipeline" >
			              <div class="c-pipeline__card" id="box3">
			                <div class="o-media" style="text-align: left;">
			                  <div class="o-media__body">
			                    <h5 class="c-pipeline__card-title">Still looking to Learn [Product Name]</h5>
			                    <p class="c-pipeline__card-subtitle">We have the Perfect Course/Training for you!</p>
			                  </div>
			                </div>
			              </div>
			            </div>
	            	</div>
	            	<div class="col-sm-6">
	            		<div class="c-pipeline" >
			              <div class="c-pipeline__card" id="box4">
			                <div class="o-media" style="text-align: left;">
			                  <div class="o-media__body">
			                    <h5 class="c-pipeline__card-title">Its Offer Day! Get [XXoff]</h5>
			                    <p class="c-pipeline__card-subtitle">on All [Brand Name] line of Products</p>
			                  </div>
			                </div>
			              </div>
			            </div>
	            	</div>
	            	<div class="col-sm-6">
	            		<div class="c-pipeline" >
			              <div class="c-pipeline__card" id="box5">
			                <div class="o-media" style="text-align: left;">
			                  <div class="o-media__body">
			                    <h5 class="c-pipeline__card-title">We Want to help you with [Keyword]</h5>
			                    <p class="c-pipeline__card-subtitle">Kick Start your [Training/Learning] with [XXOff]</p>
			                  </div>
			                </div>
			              </div>
			            </div>
	            	</div>
	            	<div class="col-sm-6">
	            		<div class="c-pipeline" >
			              <div class="c-pipeline__card" id="box6">
			                <div class="o-media" style="text-align: left;">
			                  <div class="o-media__body">
			                    <h5 class="c-pipeline__card-title">Free For First 100 Customers</h5>
			                    <p class="c-pipeline__card-subtitle">[Product Name] - The Ultimate [Keywords]</p>
			                  </div>
			                </div>
			              </div>
			            </div>
	            	</div>
	            	<div class="col-sm-6">
	            		<div class="c-pipeline" >
			              <div class="c-pipeline__card" id="box7">
			                <div class="o-media" style="text-align: left;">
			                  <div class="o-media__body">
			                    <h5 class="c-pipeline__card-title">Looking for a [Product/Course/Brand Name]</h5>
			                    <p class="c-pipeline__card-subtitle">Click here to Get Started Today!</p>
			                  </div>
			                </div>
			              </div>
			            </div>
	            	</div>
	            	<div class="col-sm-6">
	            		<div class="c-pipeline" >
			              <div class="c-pipeline__card" id="box8">
			                <div class="o-media" style="text-align: left;">
			                  <div class="o-media__body">
			                    <h5 class="c-pipeline__card-title">Use Coupon Code [xxx] for Instant [XXoff]</h5>
			                    <p class="c-pipeline__card-subtitle">Offer Valid on [Product Name] for next [XXhrs]</p>
			                  </div>
			                </div>
			              </div>
			            </div>
	            	</div>
	            	<div class="col-sm-6">
	            		<div class="c-pipeline" >
			              <div class="c-pipeline__card" id="box9">
			                <div class="o-media" style="text-align: left;">
			                  <div class="o-media__body">
			                    <h5 class="c-pipeline__card-title">We are Celebrating our [XXX] Susbcriber!</h5>
			                    <p class="c-pipeline__card-subtitle">Use Coupon Code [XX] Get [XXoff] All Day</p>
			                  </div>
			                </div>
			              </div>
			            </div>
	            	</div>
	            	<div class="col-sm-6">
	            		<div class="c-pipeline" >
			              <div class="c-pipeline__card" id="box10">
			                <div class="o-media" style="text-align: left;">
			                  <div class="o-media__body">
			                    <h5 class="c-pipeline__card-title">No! We are not Crazy! This is the best Deal Ever :)</h5>
			                    <p class="c-pipeline__card-subtitle">[Limited Time] Click here to Claim This Deal Now!</p>
			                  </div>
			                </div>
			              </div>
			            </div>
	            	</div>
	            	<div class="col-sm-6">
	            		<div class="c-pipeline" >
			              <div class="c-pipeline__card" id="box11">
			                <div class="o-media" style="text-align: left;">
			                  <div class="o-media__body">
			                    <h5 class="c-pipeline__card-title">Still Struggling With [Keyword]</h5>
			                    <p class="c-pipeline__card-subtitle">We have the Perfect Solution for you!</p>
			                  </div>
			                </div>
			              </div>
			            </div>
	            	</div>
	            	<div class="col-sm-6">
	            		<div class="c-pipeline" >
			              <div class="c-pipeline__card" id="box12">
			                <div class="o-media" style="text-align: left;">
			                  <div class="o-media__body">
			                    <h5 class="c-pipeline__card-title">Thank you! We have received your Order.</h5>
			                    <p class="c-pipeline__card-subtitle">Oh!Still Not Ordered? Claim This [Offer Name] Today!</p>
			                  </div>
			                </div>
			              </div>
			            </div>
	            	</div>
	            	<div class="col-sm-6">
	            		<div class="c-pipeline" >
			              <div class="c-pipeline__card" id="box13">
			                <div class="o-media" style="text-align: left;">
			                  <div class="o-media__body">
			                    <h5 class="c-pipeline__card-title">Your [Gifts/Bonuses] Have been Sent!</h5>
			                    <p class="c-pipeline__card-subtitle">If you Still Havn't Received your [Gift/Bonus] Claim it Now!</p>
			                  </div>
			                </div>
			              </div>
			            </div>
	            	</div>
	            	<div class="col-sm-6">
	            		<div class="c-pipeline" >
			              <div class="c-pipeline__card" id="box14">
			                <div class="o-media" style="text-align: left;">
			                  <div class="o-media__body">
			                    <h5 class="c-pipeline__card-title">[XXX] Level Deals Actiavated!</h5>
			                    <p class="c-pipeline__card-subtitle">Only Available for 1st 100 Customers. Claim Now!</p>
			                  </div>
			                </div>
			              </div>
			            </div>
	            	</div>
	            	<div class="col-sm-6">
	            		<div class="c-pipeline" >
			              <div class="c-pipeline__card" id="box15">
			                <div class="o-media" style="text-align: left;">
			                  <div class="o-media__body">
			                    <h5 class="c-pipeline__card-title">Most Recomended [Course/Training Name/ Product] is FREE for Limited Time</h5>
			                    <p class="c-pipeline__card-subtitle">Don't Miss this Exclsuive Offer. Valid for next [XXhrs]</p>
			                  </div>
			                </div>
			              </div>
			            </div>
	            	</div>
	            	<div class="col-sm-6">
	            		<div class="c-pipeline" >
			              <div class="c-pipeline__card" id="box16">
			                <div class="o-media" style="text-align: left;">
			                  <div class="o-media__body">
			                    <h5 class="c-pipeline__card-title">Still looking to Learn [Product Name]</h5>
			                    <p class="c-pipeline__card-subtitle">We have the Perfect Course/Training for you!</p>
			                  </div>
			                </div>
			              </div>
			            </div>
	            	</div>
	            	<div class="col-sm-6">
	            		<div class="c-pipeline" >
			              <div class="c-pipeline__card" id="box17">
			                <div class="o-media" style="text-align: left;">
			                  <div class="o-media__body">
			                    <h5 class="c-pipeline__card-title">Still looking to Learn [Product Name]</h5>
			                    <p class="c-pipeline__card-subtitle">We have the Perfect Course/Training for you!</p>
			                  </div>
			                </div>
			              </div>
			            </div>
	            	</div>
	            </div>
            </div>
	        </div>
	      </div><!-- // .c-modal__content -->
	    </div><!-- // .c-modal__dialog -->
	  </div>

		<!-- Main JavaScript -->
		<?php $this->load->view('bars/js');?>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#table_dt').DataTable();
			} );
			// c-pipeline__card

			$( "#modal2 .c-pipeline__card" ).click(function() {

				 row_id = $(this).attr('id');
			  $('#title').val($('#'+row_id+' h5').html());
			  $('#message').html($('#'+row_id+' p').html());
			  $('#modal2').modal('hide');
			});
		</script>
	</body>
</html>