<!doctype html>
<html lang="en">
	<head>
		<?php $this->load->view('bars/head');?>
	</head>
	<body>

		<div class="o-page">
			<div class="o-page__sidebar js-page-sidebar">
				<?php $this->load->view('bars/sidebar');?>
			</div>

			<main class="o-page__content">
				<?php $this->load->view('bars/header');?>

				<div class="container">
					<div class="row">
						<div class="o-page__card">
					    <div class="c-card c-card--center">
					      <span class="c-icon c-icon--large u-mb-small">
					        	<img class="ic-size" src="<?php echo base_url();?>assets/img/jv.png">
					      </span>	
					      <h4 class="u-mb-medium" style="margin-bottom: 20px!important; margin-top: 20px !important;">Add New User</h4>		

					      <form id="login_form">
			            <div class="c-field">
			              <label class="c-field__label">Name</label>
			              <input class="c-input u-mb-small" type="name" placeholder="e.g. Adam sandler" id="name" required>
			            </div>
			            <div class="c-field">
			              <label class="c-field__label">Email Address</label>
			              <input class="c-input u-mb-small" type="email" placeholder="e.g. adam@sandler.com" id="email" required>
			            </div>
			            <div class="c-field">
			              <label class="c-field__label">Password</label>
			              <input class="c-input u-mb-small" type="password" onkeyup="checkPasswordStrength();" placeholder="Numbers, Pharagraphs Only" id="password" required>
			              <div id="password-strength-status"></div>
			            </div>
			            <div class="c-field">
			              <label class="c-field__label">Re-Password</label>
			              <input class="c-input u-mb-small" type="password" placeholder="Numbers, Pharagraphs Only" id="re_password" required>
			            </div>
			            <button class="c-btn c-btn--fullwidth c-btn--info bt_pass" disabled>Save</button>
		          	</form>				      
					    </div>
						</div>
					</div>
					<?php $this->load->view('bars/footer');?>
				</div>
			</main>
		</div>
		<!-- Main JavaScript -->
		<?php $this->load->view('bars/js');?>
		<script type="text/javascript">
			$( "#login_form" ).submit(function( event ) {
        register_user_resaller();
        event.preventDefault();
      });
      <?php if($this->session->flashdata('Message')) { ?>
        toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "100",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "show",
                "hideMethod": "hide",
                "positionClass": "toast-bottom-right",
            };
            toastr.success("<?php echo $this->session->flashdata('Message'); ?>");
        <?php };?>
		</script>

	</body>
</html>