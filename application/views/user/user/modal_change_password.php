<div class="c-modal__dialog modal-dialog" role="document">
  <div class="c-modal__content">
    <div class="c-modal__body">
      <span class="c-modal__close" data-dismiss="modal" aria-label="Close">
        <i class="feather icon-x"></i>
      </span>

      <span class="c-icon c-icon--large u-mb-small">
        <i class="feather icon-anchor"></i>
      </span>
      <h3 class="u-mb-small">Update Password</h3>
      
      <p class="u-mb-medium">Email Address :- <b><?php echo $user->email;?></b></p>
      <div class="c-field u-mb-medium">
          <label class="c-field__label" for="password">New Password</label>
          <input class="c-input" type="text" id="password">
          <input type="hidden" id="user_id" value="<?php echo $user->id; ?>" name="">
        </div>
      <div class="o-line">
        <a href="javascript:void(0);" class="c-btn c-btn--info" onclick="update_user_password_by_admin();">Update</a>
      </div>
    </div>
  </div><!-- // .c-modal__content -->
</div><!-- // .c-modal__dialog -->