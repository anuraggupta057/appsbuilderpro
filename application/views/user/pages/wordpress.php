<!doctype html>
<?php $login_data =  $this->session->userdata('login_data'); ?>
<html lang="en">
	<head>
		<?php $this->load->view('bars/head');?>
	</head>
	<body>

		<div class="o-page">
			<div class="o-page__sidebar js-page-sidebar">
				<?php $this->load->view('bars/sidebar');?>
			</div>

			<main class="o-page__content">
				<?php $this->load->view('bars/header');?>

				<div class="container">
					<div class="row">
		        <div class="col-md-12">
              <div class="c-alert c-alert--info u-mb-medium">
                <span class="c-alert__icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg>
                </span>

                <div class="c-alert__content">
                  <h4 class="c-alert__title">Wordpress</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci consequuntur, ipsam laboriosam alias reiciendis mollitia dolores ipsum dolor sit amet, consectetur adipisicing elit. Adipisci consequuntur, ipsam laboriosam alias reiciendis mollitia dolores!</p>
                </div>
              </div><!-- // .c-alert -->
            </div>
		      </div>
					<?php $this->load->view('bars/footer');?>
				</div>
			</main>
		</div>

		<!-- Main JavaScript -->
		<?php $this->load->view('bars/js');?>
		<script type="text/javascript">
      
      <?php if($this->session->flashdata('Message')) { ?>
        toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "100",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "show",
                "hideMethod": "hide",
                "positionClass": "toast-bottom-right",
            };
            toastr.success("<?php echo $this->session->flashdata('Message'); ?>");
        <?php };?>
    </script>
	</body>
</html>