<!doctype html>
<html lang="en">
	<head>
		<?php $this->load->view('bars/head');?>
	</head>
	<body>
		<div class="o-page">
			<div class="o-page__sidebar js-page-sidebar">
				<?php $this->load->view('bars/sidebar');?>
			</div>
			<main class="o-page__content">
				<?php $this->load->view('bars/header');?>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="c-card c-card--center">
								<div class="c-table-responsive@wide">
									<table id="table_dt" class="display c-table" style="width:100%">
										<thead class="c-table__head">
											<tr class="c-table__row">
												<th class="c-table__cell c-table__cell--head">Sr.No.</th>
												<th class="c-table__cell c-table__cell--head">Url</th>
												<th class="c-table__cell c-table__cell--head">Download</th>
												<th class="c-table__cell c-table__cell--head">Notification</th>
												<th class="c-table__cell c-table__cell--head">Delete</th>
											</tr>
											
										</thead>
										<tbody>
											<?php $x=1; foreach ($manifest as $key ) { ?>
											<tr class="c-table__row">
												<td class="c-table__cell"><?php echo $x++; ?></td>
												<td class="c-table__cell"><?php echo $key->url; ?></td>
												<td class="c-table__cell"><a href="<?php echo base_url(); ?>user/apps/download_files/<?php echo $key->id;?>" target="_blank" class="c-btn c-btn--secondary c-btn--small">Download</a></td>
												<td class="c-table__cell"><a href="javascript:void(0);" class="c-btn c-btn--warning c-btn--small">View</a></td>
												<td class="c-table__cell"><a href="javascript:void(0);" class="c-btn c-btn--danger c-btn--small">Delete</a></td>
											</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<?php $this->load->view('bars/footer');?>
				</div>
			</main>
		</div>
		<!-- Main JavaScript -->
		<?php $this->load->view('bars/js');?>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
		    $('#table_dt').DataTable();
			});
		</script>
	</body>
</html>