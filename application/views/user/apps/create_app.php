<!doctype html>
<html lang="en">
	<head>
		<?php $this->load->view('bars/head');?>
		<style type="text/css">
			.set_mobile{
				position: relative;
				width: 408px;
				margin:0 auto;
			}
			.set_mobile img{
				position: absolute;
				top: 0px;
				left: 0px;
				width: 408px;
				margin:0 auto;
			}
			.set_ifream{
				position: relative;
				width: 408px;
				margin:0 auto;
			}
			.set_ifream iframe{
		    position: absolute;
		    top: 49px;
		    left: 17px;
		    width: 372px;
		    margin: 0 auto;
		    overflow: hidden;
		    height: 737px;
		    border:none;
			}
			#result .c-card--center{
				height: 800px;
			}
		</style>
	</head>
	<body>

		<div class="o-page">
			<div class="o-page__sidebar js-page-sidebar">
				<?php $this->load->view('bars/sidebar');?>
			</div>

			<main class="o-page__content">
				<?php $this->load->view('bars/header');?>

				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="c-progress c-progress--info">
	              <div class="c-progress__bar" style="width:5%;"></div>
	            </div>
						</div>
						<div class="col-sm-6">
							<div id="result">
								<div class="">
									<div class=" c-card--center">
										<div class="set_mobile">
											<img src="<?php echo base_url(); ?>assets/img/phone.png" class="img-fluid">
										</div>
										<div class="set_ifream">
											<iframe src="" scrolling="no" width="100%"></iframe>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="o-page__card">
								<div class="c-card c-card--center">
									<span class="c-icon c-icon--large u-mb-small">
										<img class="ic-size" src="<?php echo base_url(); ?>assets/img/jv.png">
									</span>
									<h4 class="u-mb-medium" style="margin-bottom: 14px!important;
									margin-top: 20px !important;">Enter Website URL</h4>
									<form role="form" method="GET" action="<?php echo base_url(); ?>user/apps/generate_manifest">
										<div class="c-field form-group">
											<input id="url" class="c-input u-mb-small" type="url" pattern="https://.*" title="Please enter https: url" name="url" value="" required="" onchange="check_website();" onkeyup="check_website();">
										</div>
										<button type="submit" id="btn_submit" class="c-btn c-btn--fullwidth c-btn--info" disabled>Get Started</button>
									</form>
								</div>
							</div>
						</div>
						
						
					</div>
					
					<?php $this->load->view('bars/footer');?>
				</div>
			</main>
		</div>

		<!-- Main JavaScript -->
		<?php $this->load->view('bars/js');?>

		<script type="text/javascript">
			function check_website() {
				input_box = $('#url').val();
				$('#result .set_ifream').html('<iframe src="'+input_box+'" scrolling="no" width="100%" title="Iframe Example"></iframe>');
				$('#btn_submit').html('Get Started <i class="fas fa-spinner fa-spin"></i>');
				$('#btn_submit').attr('disabled',true);
				setTimeout(function () { 
					$('#btn_submit').html('Get Started');
					$('#btn_submit').attr('disabled',false);

				}, 3000);
			}
		</script>

	</body>
</html>