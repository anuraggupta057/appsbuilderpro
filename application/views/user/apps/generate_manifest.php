<!doctype html>
<html lang="en">
	<head>
		<?php $this->load->view('bars/head');?>
		<style type="text/css">
			.set_mobile{
				position: relative;
				width: 408px;
				margin:0 auto;
			}
			.set_mobile img{
				position: absolute;
				top: 0px;
				left: 0px;
				width: 408px;
				margin:0 auto;
			}
			.set_ifream{
				position: relative;
				width: 408px;
				margin:0 auto;
			}
			.set_ifream iframe{
		    position: absolute;
		    top: 49px;
		    left: 17px;
		    width: 372px;
		    margin: 0 auto;
		    overflow: hidden;
		    height: 737px;
		    border:none;
			}
			#result .c-card--center{
				height: 800px;
			}
		</style>
	</head>
	<body>

		<div class="o-page">
			<div class="o-page__sidebar js-page-sidebar">
				<?php $this->load->view('bars/sidebar');?>
			</div>

			<main class="o-page__content">
				<?php $this->load->view('bars/header');?>

				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="c-progress c-progress--info">
	              <div class="c-progress__bar" style="width:33%;"></div>
	            </div>
						</div>						
						<div class="col-sm-6">
							<div id="result">
								<div class="">
									<div class=" c-card--center">
										<div class="set_mobile">
											<img src="<?php echo base_url(); ?>assets/img/phone.png" class="img-fluid">
										</div>
										<div class="set_ifream">
											<iframe src="<?php echo $_GET['url']?>" scrolling="no" width="100%"></iframe>
										</div>
									</div>
								</div>
							</div>
						</div>	
						<div class="col-sm-6">
							<div class="o-page__card" >
							    <div class="c-card c-card--center" >
							      	<span class="c-icon c-icon--large u-mb-small">
										<img class="ic-size" src="<?php echo base_url(); ?>assets/img/jv.png">
							      	</span>
							      	<h4 class="u-mb-medium">Enter Details for your App</h4>
							      	<form role="form" method="POST" action="<?php echo base_url(); ?>user/apps/manifest_store" enctype="multipart/form-data">
								        <div class="c-field">
								          	<label for="name" class="c-field__label control-label">Name</label>
								          	<input id="name" class="c-input u-mb-small" type="text" name="name" value="<?php if(isset($url_data['title'])){echo $url_data['title']; }?>" required="">
								        </div>
								        <div class="c-field">
								          	<label for="shortname" class="c-field__label control-label">Short Name</label>
								          	<input id="shortname" class="c-input u-mb-small" type="text" name="shortname" value="<?php if(isset($url_data['title'])){echo $url_data['title']; }?>" required="">
								        </div>
								        <div class="c-field">
								          	<label for="description" class="c-field__label control-label">Description</label>
								          	<textarea id="description" class="c-input u-mb-small" type="text" name="description" required=""><?php if(isset($url_data['metaTags']['description'][0])){echo $url_data['metaTags']['description'][0]; }?></textarea>
								        </div>
							        	<div class="c-field">
							          		<label for="imageurl" class="c-field__label control-label">Upload an image</label>
							          		<input id="imageurl" accept="image/*" onchange="show_item_img('imageurl','output_image')" class="c-input u-mb-small" type="file" name="image" required="">
							        	</div>
								        <div class="c-field row">
								          	<div class="col-lg-8">
								            	<label class="c-field__label control-label">Preview</label>
								          	</div>
								          	<div class="col-lg-8">
								            	<img id="output_image" src="" alt="" width="100" height="100">
								          	</div>
									        <div class="col-lg-4">
									        </div>
								        </div>
							        	<div class="c-field">
							          		<label for="start_url" class="c-field__label control-label">Start Url</label>
							          		<input id="start_url" class="c-input u-mb-small" type="text" name="start_url" value="<?php echo $_GET['url']?>" required="">
							        	</div>
							        	<div class="c-field">
							          		<select style="display:none" id="display" class="c-input u-mb-small" name="display" value="Browser">
									            <option value="Browser">Browser</option>
									            <option value="Fullscreen">Fullscreen</option>
									            <option value="Standalone">Standalone</option>
									            <option value="Minimal-ui">Minimal-ui</option>
									        </select>
							        	</div>
							        	
							        	<button type="submit" name="next" class="c-btn c-btn--fullwidth c-btn--info">Next</button>
							      </form>
							  	</div>
							</div>
						</div>
					</div>
					<?php $this->load->view('bars/footer');?>
				</div>
			</main>
		</div>

		<!-- Main JavaScript -->
		<?php $this->load->view('bars/js');?>
	</body>
</html>