<!doctype html>
<html lang="en">
	<head>
		<?php $this->load->view('bars/head');?>
	</head>
	<body>

		<div class="o-page">
			<div class="o-page__sidebar js-page-sidebar">
				<?php $this->load->view('bars/sidebar');?>
			</div>

			<main class="o-page__content">
				<?php $this->load->view('bars/header');?>

				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="c-progress c-progress--info">
	              <div class="c-progress__bar" style="width:100%;"></div>
	            </div>
						</div>
						<div class="o-page__card">
						    <div class="c-card c-card--center">
						      <span class="c-icon c-icon--large u-mb-small">
						        	<img class="ic-size" src="<?php echo base_url();?>assets/img/jv.png">
						      </span>
						      <div class="text-center">
						      	<img class="img-fluid" style="max-width: 200px;" src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=<?php echo $manifest->url; ?>%2F&choe=UTF-8">
						      </div>
						        	
						      <h4 class="u-mb-medium" style="margin-bottom: 20px!important; margin-top: 20px !important;">Your Progressive App is Now Ready!</h4>						      
						        <a href="<?php echo base_url();?>user/apps/download_files/<?php echo $id; ?>" class="c-btn c-btn--fullwidth c-btn--info">Download</a>						      
						    </div>
						</div>
					</div>
					<?php $this->load->view('bars/footer');?>
				</div>
			</main>
		</div>
		<!-- Main JavaScript -->
		<?php $this->load->view('bars/js');?>
	</body>
</html>