<?php $login_plan = $this->session->userdata('login_plan'); ?>
<!doctype html>
<html lang="en">
	<head>
		<?php $this->load->view('bars/head');?>
		<style type="text/css">
			.set_mobile{
				position: relative;
				width: 408px;
				margin:0 auto;
			}
			.set_mobile img{
				position: absolute;
				top: 0px;
				left: 0px;
				width: 408px;
				margin:0 auto;
			}
			.set_ifream{
				position: relative;
				width: 408px;
				margin:0 auto;
			}
			.set_ifream iframe{
		    position: absolute;
		    top: 49px;
		    left: 17px;
		    width: 372px;
		    margin: 0 auto;
		    overflow: hidden;
		    height: 737px;
		    border:none;
			}
			#result .c-card--center{
				height: 800px;
			}
		</style>
	</head>
	<body>

		<div class="o-page">
			<div class="o-page__sidebar js-page-sidebar">
				<?php $this->load->view('bars/sidebar');?>
			</div>

			<main class="o-page__content">
				<?php $this->load->view('bars/header');?>

				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="c-progress c-progress--info">
	              <div class="c-progress__bar" style="width:66%;"></div>
	            </div>
						</div>						
						<div class="col-sm-6">
							<div id="result">
								<div class="">
									<div class=" c-card--center">
										<div class="set_mobile">
											<img src="<?php echo base_url(); ?>assets/img/phone.png" class="img-fluid">
										</div>
										<div class="set_ifream">
											<iframe src="<?php echo $manifest->url;?>" scrolling="no" width="100%"></iframe>
										</div>
									</div>
								</div>
							</div>
						</div>	
						<div class="col-sm-6">
							<div class="o-page__card" >
						    <div class="c-card c-card--center" >
					      	<span class="c-icon c-icon--large u-mb-small">
										<img class="ic-size" src="<?php echo base_url(); ?>assets/img/jv.png">
					      	</span>
					      	<h4 class="u-mb-medium">Enter Details for your App</h4>
					      	<form role="form" method="POST" action="<?php echo base_url(); ?>user/apps/setup_manifest_update" enctype="multipart/form-data">
					      		<div class="u-block u-mb-xsmall" style="text-align: left;">
				              <label class="c-switch u-mr-small">
				                <input class="c-switch__input" id="switch1" type="checkbox" name="gdpr" onclick="change_box_status('gdpr');">
				                <span class="c-switch__label">GDPR</span>
				              </label>
				            </div>
				            <div class="u-block u-mb-xsmall" style="text-align: left;">
				              <label class="c-switch u-mr-small">
				                <input class="c-switch__input" id="switch2" type="checkbox" name="facebook" onclick="change_box_status('facebook');"  <?php if (!in_array("2", $login_plan)) { echo "disabled"; }?>>
				                <span class="c-switch__label">Facebook Pixel</span>
				              </label>
				            </div>
				            <div class="u-block u-mb-xsmall" style="text-align: left;">
				              <label class="c-switch u-mr-small">
				                <input class="c-switch__input" id="switch3" type="checkbox" name="google" onclick="change_box_status('google');" <?php if (!in_array("2", $login_plan)) { echo "disabled"; }?>>
				                <span class="c-switch__label">Google Tag Manager</span>
				              </label>
				            </div>
				            <div class="u-block u-mb-xsmall" style="text-align: left;">
				              <label class="c-switch u-mr-small">
				                <input class="c-switch__input" id="switch4" type="checkbox" name="menu" onclick="change_box_status('menu');" <?php if (!in_array("2", $login_plan)) { echo "disabled"; }?>>
				                <span class="c-switch__label">Menus</span>
				              </label>
				            </div>
				            <div class="c-field" style="display: none;" id="gdpr_box">
					          	<label for="gdpr_content" class="c-field__label control-label">GDPR</label>
					          	<textarea id="gdpr_content" class="c-input u-mb-small" type="text" name="gdpr_content" required="">This website uses cookies to ensure you get the best experience on our website.</textarea>
						        </div>
						        <?php if (in_array("2", $login_plan)) { ?>
						        <div class="c-field" style="display: none;" id="facebook_box">
					          	<label for="facebook_content" class="c-field__label control-label">Facebook Pixel Code</label>
					          	<input type="text" class="c-input u-mb-small" id="facebook_content" name="facebook_content">
						        </div>	
						        <div class="c-field" style="display: none;" id="google_box">
					          	<label for="google_content" class="c-field__label control-label">Google Tag Manager</label>
					          	<input type="text" class="c-input u-mb-small" id="google_content" name="google_content">
						        </div>	
						        <div class="c-field" style="display: none;" id="menu_box">
						        	<h5>Menu Box 1</h5>
						        	<div class="c-field">
						          	<div class="row">
							        		<div class="col-sm-3">
							        			<label for="icon1" class="c-field__label control-label">Menu Icon 1</label>
							        		</div>
							        		<div class="col-sm-9">
							        			<input type="text" class="c-input u-mb-small" id="icon1" name="icon1">
							        		</div>
							        	</div>
							        </div>
							        <div class="c-field">
							        	<div class="row">
							        		<div class="col-sm-3">
							        			<label for="murl1" class="c-field__label control-label">Menu Url 1</label>
							        		</div>
							        		<div class="col-sm-9">
							        			<input type="text" class="c-input u-mb-small" id="murl1" name="murl1">
							        		</div>
							        	</div>
							        </div>
							        <h5>Menu Box 2</h5>
							        <div class="c-field">
						        		<div class="row">
						        			<div class="col-sm-3">
							        			<label for="icon2" class="c-field__label control-label">Menu Icon 2</label>
							        		</div>
							        		<div class="col-sm-9">
							        			<input type="text" class="c-input u-mb-small" id="icon2" name="icon2">
							        		</div>
							        	</div>
							        </div>
							        <div class="c-field">
							        	<div class="row">
							        		<div class="col-sm-3">
							        			<label for="murl2" class="c-field__label control-label">Menu Url 2</label>
							        		</div>
							        		<div class="col-sm-9">
							        			<input type="text" class="c-input u-mb-small" id="murl2" name="murl2">
							        		</div>
							        	</div>
							        </div>
							        <h5>Menu Box 3</h5>
							        <div class="c-field">
						          	<div class="row">
							        		<div class="col-sm-3">
							        			<label for="icon3" class="c-field__label control-label">Menu Icon 3</label>
							        		</div>
							        		<div class="col-sm-9">
							        			<input type="text" class="c-input u-mb-small" id="icon3" name="icon3">
							        		</div>
							        	</div>
							        </div>
							        <div class="c-field">
							        	<div class="row">
							        		<div class="col-sm-3">
							        			<label for="murl3" class="c-field__label control-label">Menu Url 3</label>
							        		</div>
							        		<div class="col-sm-9">
							        			<input type="text" class="c-input u-mb-small" id="murl3" name="murl3">
							        		</div>
							        	</div>
							        </div>
							        <h5>Menu Box 4</h5>
							        <div class="c-field">
						          	<div class="row">
							        		<div class="col-sm-3">
							        			<label for="icon4" class="c-field__label control-label">Menu Icon 4</label>
							        		</div>
							        		<div class="col-sm-9">
							        			<input type="text" class="c-input u-mb-small" id="icon4" name="icon4">
							        		</div>
							        	</div>
							        </div>
							        <div class="c-field">
							        	<div class="row">
							        		<div class="col-sm-3">
							        			<label for="murl4" class="c-field__label control-label">Menu Url 4</label>
							        		</div>
							        		<div class="col-sm-9">
							        			<input type="text" class="c-input u-mb-small" id="murl4" name="murl4">
							        		</div>
							        	</div>
							        </div>
						        </div>	
						        <?php } ?>
						        
					      		<input type="hidden" id="file_id" name="file_id" value="<?php echo $manifest->id; ?>">
					        	<button type="submit" name="next" class="c-btn c-btn--fullwidth c-btn--info">Next</button>
						      </form>
						  	</div>
							</div>
						</div>
					</div>
					<?php $this->load->view('bars/footer');?>
				</div>
			</main>
		</div>

		<!-- Main JavaScript -->
		<?php $this->load->view('bars/js');?>
		<script type="text/javascript">
			function	change_box_status(type)
			{
				if (type == 'gdpr') 
				{
					if ($('#switch1').is(':checked')) 
					{
						$('#gdpr_box').show();
					}
					else
					{
						$('#gdpr_box').hide();
					}
				}
				if (type == 'facebook') 
				{
					if ($('#switch2').is(':checked')) 
					{
						$('#facebook_box').show();
					}
					else
					{
						$('#facebook_box').hide();
					}
				}
				if (type == 'google') 
				{
					if ($('#switch3').is(':checked')) 
					{
						$('#google_box').show();
					}
					else
					{
						$('#google_box').hide();
					}
				}		
				if (type == 'menu') 
				{
					if ($('#switch4').is(':checked')) 
					{
						$('#menu_box').show();
					}
					else
					{
						$('#menu_box').hide();
					}
				}			
			}
		</script>
	</body>
</html>