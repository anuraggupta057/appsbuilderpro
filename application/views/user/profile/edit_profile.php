<!doctype html>
<?php $login_data =  $this->session->userdata('login_data'); ?>
<html lang="en">
	<head>
		<?php $this->load->view('bars/head');?>
	</head>
	<body>

		<div class="o-page">
			<div class="o-page__sidebar js-page-sidebar">
				<?php $this->load->view('bars/sidebar');?>
			</div>

			<main class="o-page__content">
				<?php $this->load->view('bars/header');?>

				<div class="container">
					<div class="row">
						<div class="col-12">
							<nav class="c-tabs"> 
								<div class="c-tabs__list nav nav-tabs" id="myTab" role="tablist">
									<a class="c-tabs__link <?php if(!isset($_GET['password_change'])){echo 'active';} ?>" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
										<span class="c-tabs__link-icon">
											<i class="feather icon-settings"></i>
										</span>Account Settings
									</a>
									<a class="c-tabs__link <?php if(isset($_GET['password_change'])){echo 'active';} ?>" id="nav-home-tab1" data-toggle="tab" href="#nav-home1" role="tab" aria-controls="nav-home1" aria-selected="true">
										<span class="c-tabs__link-icon">
											<i class="feather icon-settings"></i>
										</span>Password Update
									</a>
								</div>
								<div class="c-tabs__content tab-content" id="nav-tabContent">
									<div class="c-tabs__pane <?php if(!isset($_GET['password_change'])){echo 'active';} ?>" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
										<div class="row">
											<div class="col-xl-4">
												<div class="c-field u-mb-medium">
													<label class="c-field__label" for="name">Name</label>
													<input class="c-input" type="text" id="name" value="<?php echo $login_data->name; ?>">
												</div>
												<div class="c-field u-mb-medium">
													<label class="c-field__label" for="email">Email Address</label>
													<input class="c-input" type="text" id="email" value="<?php echo $login_data->email; ?>" disabled>
												</div>
											</div>
											<div class="col-xl-4">
												<div class="c-field u-mb-medium">
													<label class="c-field__label" for="country">Country</label>
													<div class="c-select">
														<select class="c-select__input" id="country">
															<option <?php if ($login_data->name == 'Australia'){ echo "selected";	} ?>>Australia</option>
															<option <?php if ($login_data->name == 'Singapore'){ echo "selected";	} ?>>Singapore</option>
															<option <?php if ($login_data->name == 'United Stated'){ echo "selected";	} ?>>United Stated</option>
															<option <?php if ($login_data->name == 'United Kingdom'){ echo "selected";	} ?>>United Kingdom</option>
														</select>
													</div>
												</div>
												<div class="c-field u-mb-medium">
													<label class="c-field__label" for="img">Image</label>
													<input class="c-input" type="file" id="img" onchange="show_item_img('img','img_pre');">
												</div>
											</div>
											<div class="col-xl-4">
												<div class="c-card u-text-center">
													<div class="c-avatar u-inline-flex">
														<?php 
														$img = base_url().'assets/img/user.png';
														if ($login_data->img != ''){ 
															 $img = base_url().'files/user_img/'.$login_data->img;
														}?>
															
														<img class="c-avatar__img" id="img_pre" src="<?php echo $img; ?>" alt="<?php echo $login_data->name; ?>">
													</div>
													<h5><?php echo $login_data->name; ?></h5>
													<p class="u-pb-small u-mb-small">Freelance Web Developer</p>
												</div>
												
											</div>
										</div>

										<span class="c-divider u-mv-medium"></span>

										<div class="row">
											<div class="col-12 col-sm-7 col-xl-2 u-mr-auto u-mb-xsmall">
												<button class="c-btn c-btn--info c-btn--fullwidth" onclick="update_profile();">Update Profile</button>
											</div>
										</div>
									</div>
									<div class="c-tabs__pane <?php if(isset($_GET['password_change'])){echo 'active';} ?>" id="nav-home1" role="tabpanel" aria-labelledby="nav-home-tab1">
										<div class="row">
											<div class="col-xl-4">
												<div class="c-field u-mb-medium">
													<label class="c-field__label" for="password">New Password</label>
													<input class="c-input" type="text" id="password" onkeyup="checkPasswordStrength();">
													<div id="password-strength-status"></div>
												</div>
											</div>
										</div>

										<span class="c-divider u-mv-medium"></span>

										<div class="row">
											<div class="col-12 col-sm-7 col-xl-2 u-mr-auto u-mb-xsmall">
												<button class="c-btn c-btn--info c-btn--fullwidth bt_pass" onclick="update_password();" disabled>Update Password</button>
											</div>
										</div>
									</div>
								</div>
							</nav>
						</div>
					</div>
					<?php $this->load->view('bars/footer');?>
				</div>
			</main>
		</div>

		<!-- Main JavaScript -->
		<?php $this->load->view('bars/js');?>
		<script type="text/javascript">
      
      <?php if($this->session->flashdata('Message')) { ?>
        toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "100",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "show",
                "hideMethod": "hide",
                "positionClass": "toast-bottom-right",
            };
            toastr.success("<?php echo $this->session->flashdata('Message'); ?>");
        <?php };?>
    </script>
	</body>
</html>