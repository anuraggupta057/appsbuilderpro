# Configuring the manifest in your site

1. Upload all files to root of your website

2. Copy following lines of code to your <head> tag


<link rel="manifest" href="manifest.json"></link>
<script src="https://www.gstatic.com/firebasejs/5.0.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.0.0/firebase-messaging.js"></script>
<script type="text/javascript" src="abp-custom.js"></script>
<script type="text/javascript" src="manup.js"></script>
<script type="text/javascript" src="abp-sw-register.js"></script>
