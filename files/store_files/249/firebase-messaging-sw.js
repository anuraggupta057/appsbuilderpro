importScripts("https://www.gstatic.com/firebasejs/7.16.1/firebase-app.js");
importScripts(
    "https://www.gstatic.com/firebasejs/7.16.1/firebase-messaging.js",
);
// For an optimal experience using Cloud Messaging, also add the Firebase SDK for Analytics.
importScripts(
    "https://www.gstatic.com/firebasejs/7.16.1/firebase-analytics.js",
);

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.

var firebaseConfig = {
    apiKey: "AIzaSyDc3v0-ikCx_StxhasO_PYkx0FVMYwZ_UE",
    authDomain: "appsbuilderpro-7689e.firebaseapp.com",
    databaseURL: "https://appsbuilderpro-7689e.firebaseio.com",
    projectId: "appsbuilderpro-7689e",
    storageBucket: "appsbuilderpro-7689e.appspot.com",
    messagingSenderId: "930427761797",
    appId: "1:930427761797:web:93ec274b2c1823f07be5ec",
    measurementId: "G-D1411HSJ7Z"
};

firebase.initializeApp(firebaseConfig);
// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
    console.log(payload.data.data);
    // Customize notification here
    json_data = payload.data.data;
    json_data = JSON.parse(json_data);
    const notificationTitle = json_data.title;
    const notificationOptions = {
        body: json_data.message,
        icon: json_data.payload.icon,
        click_action: "https://google.com",
    };
    
    self.addEventListener('notificationclick', function(event) {
      event.notification.close();
    
      //window.open(json_data.payload.click_action,'_blank');
    });

    return self.registration.showNotification(
        notificationTitle,
        notificationOptions,
    );
});