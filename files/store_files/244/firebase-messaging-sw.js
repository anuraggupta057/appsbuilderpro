importScripts("https://www.gstatic.com/firebasejs/7.16.1/firebase-app.js");
importScripts(
    "https://www.gstatic.com/firebasejs/7.16.1/firebase-messaging.js",
);
// For an optimal experience using Cloud Messaging, also add the Firebase SDK for Analytics.
importScripts(
    "https://www.gstatic.com/firebasejs/7.16.1/firebase-analytics.js",
);

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.

var firebaseConfig = {
    apiKey: "AIzaSyDc3v0-ikCx_StxhasO_PYkx0FVMYwZ_UE",
    authDomain: "appsbuilderpro-7689e.firebaseapp.com",
    databaseURL: "https://appsbuilderpro-7689e.firebaseio.com",
    projectId: "appsbuilderpro-7689e",
    storageBucket: "appsbuilderpro-7689e.appspot.com",
    messagingSenderId: "930427761797",
    appId: "1:930427761797:web:93ec274b2c1823f07be5ec",
    measurementId: "G-D1411HSJ7Z"
};

firebase.initializeApp(firebaseConfig);
// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
    console.log(
        "[firebase-messaging-sw.js] Received background message ",
        payload,
    );
    console.log(payload);
    // Customize notification here
    const notificationTitle = "HAI";
    const notificationOptions = {
        body: "1234.",
        icon: "/itwonders-web-logo.png",
    };

    return self.registration.showNotification(
        notificationTitle,
        notificationOptions,
    );
});