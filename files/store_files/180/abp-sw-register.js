//This is the "Offline page" service worker

//Add this below content to your HTML page, or add the js file to your page at the very top to register service worker

// Initialize Firebase
var config = {
  apiKey: "AIzaSyD2p6lWVTEbneI2gm6aheeHjTFRQdBdN2o",
  authDomain: "progressive-apps-builder.firebaseapp.com",
  databaseURL: "https://progressive-apps-builder.firebaseio.com",
  projectId: "progressive-apps-builder",
  storageBucket: "progressive-apps-builder.appspot.com",
  messagingSenderId: "692054876427"
};
firebase.initializeApp(config);
const messaging = firebase.messaging();

if (navigator.serviceWorker.controller) {
  console.log('[ABP Builder] active service worker found, no need to register')
} else {
  //Register the ServiceWorker
  navigator.serviceWorker.register('pwa-sw-v2.js', {
    scope: './'
  }).then((registration) => {
      messaging.useServiceWorker(registration);
      messaging.requestPermission()
              .then(function () {
                  console.log('Notification permission granted.');
                  return messaging.getToken();
              })
              .then(function (currentToken) {
                  var desk_token = currentToken;
                  if (desk_token != '') {
                      var urls = "https://app.progressiveappsbuilder.com/save_notification_token";
                      $.get(urls,
                        {
                            token: currentToken,
                            domain: window.location.hostname
                        },
                        function(data, status){
                            console.log(data);
                        });
                  }
                  console.log(currentToken);
              })
              .catch(function (err) {
                  console.log('Unable to get permission to notify.', err);
              });
      messaging.onMessage(function (payload) {
          return self.registration.showNotification(notificationTitle,notificationOptions);
      });
  });
}
