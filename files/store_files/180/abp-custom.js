
if (typeof(jQuery) == 'undefined') {
    //jquery not present on website
   (function() {
       // Load jquery script if doesn't exist
       var script = document.createElement("SCRIPT");
       script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js';
       script.type = 'text/javascript';
       script.onload = function() {
           pwaBuilderOverlayInit();
       };
       document.getElementsByTagName("head")[0].appendChild(script);
   })();
}else{
    //jquery present on website
   pwaBuilderOverlayInit();
}

function pwaBuilderOverlayInit(){
$(document).one("ready",function(){
    $.ajax({
        url: 'abp-overlay.html',
        success: function(response){
            $("body").append(response);
        }
    });

   });
}
