// alert($('base').attr('href'));


base_url = $('base').attr('href');
function login_check() {
	email = $("#email").val();
	password = $("#password").val();

	$.ajax({
		url: base_url + "login/check_login",
		type: "POST",
		data: { email: email, password: password },
		success: function (result) {
			console.log(result);
			if (result == "Valid") {
				window.location.href = base_url + "user/dashboard";
			} else {
				toastr.options = {
				  "closeButton": false,
				  "debug": false,
				  "newestOnTop": false,
				  "progressBar": true,
				  "preventDuplicates": true,
				  "onclick": null,
				  "showDuration": "100",
				  "hideDuration": "1000",
				  "timeOut": "5000",
				  "extendedTimeOut": "1000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "show",
				  "hideMethod": "hide",
				  "positionClass": "toast-bottom-right",
				};
				toastr.error("Email & Password wrong", "Error");
			}
		},
	});
}

function register_user()
{
	name = $("#name").val();
	email = $("#email").val();
	password = $("#password").val();
	re_password = $("#re_password").val();
	if (password == '') 
	{
		toastr.options = {
		  "closeButton": false,
		  "debug": false,
		  "newestOnTop": false,
		  "progressBar": true,
		  "preventDuplicates": true,
		  "onclick": null,
		  "showDuration": "100",
		  "hideDuration": "1000",
		  "timeOut": "5000",
		  "extendedTimeOut": "1000",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "show",
		  "hideMethod": "hide",
		  "positionClass": "toast-bottom-right",
		};
		toastr.error("Please Fill Password", "Error");
	}
	if (password != re_password) 
	{
		toastr.options = {
		  "closeButton": false,
		  "debug": false,
		  "newestOnTop": false,
		  "progressBar": true,
		  "preventDuplicates": true,
		  "onclick": null,
		  "showDuration": "100",
		  "hideDuration": "1000",
		  "timeOut": "5000",
		  "extendedTimeOut": "1000",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "show",
		  "hideMethod": "hide",
		  "positionClass": "toast-bottom-right",
		};
		toastr.error("Password And Retype Password Are Not Match", "Error");
	}
	else
	{
		$.ajax({
			url: base_url + "login/register_user",
			type: "POST",
			data: {name:name, email: email, password: password },
			success: function (result) {
				if (result == "Valid") {
					window.location.href = base_url + "user/dashboard";
				} 
				else {
					toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "100",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "show",
            "hideMethod": "hide",
            "positionClass": "toast-bottom-right",
	        };
					toastr.error("Email Already", "Error");
				}
			},
		});
	}	
}

function forgot_password()
{
	email = $("#email").val();
	
	if (email == '') 
	{
		toastr.options = {
		  "closeButton": false,
		  "debug": false,
		  "newestOnTop": false,
		  "progressBar": true,
		  "preventDuplicates": true,
		  "onclick": null,
		  "showDuration": "100",
		  "hideDuration": "1000",
		  "timeOut": "5000",
		  "extendedTimeOut": "1000",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "show",
		  "hideMethod": "hide",
		  "positionClass": "toast-bottom-right",
		};
		toastr.error("Please Fill Email Address", "Error");
	}
	else
	{
		$('#btn_submit').attr('disabled',true);
		$('#btn_submit').html('Submit <i class="fas fa-spinner fa-spin"></i>');
		$.ajax({
			url: base_url + "login/forgot_password",
			type: "POST",
			data: {email: email},
			success: function (result) {
				if (result == "Valid") {
					toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "100",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "show",
            "hideMethod": "hide",
            "positionClass": "toast-bottom-right",
	        };
					toastr.success("Email Send successfully");
				} 
				else {
					toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "100",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "show",
            "hideMethod": "hide",
            "positionClass": "toast-bottom-right",
	        };
					toastr.error("Email Are Not Register user", "Error");
				}

				$('#btn_submit').html('Submit');
				$('#btn_submit').attr('disabled',false);
			},
		});
	}	
}

function password_recover()
{
	user_id = $("#user_id").val();
	email = $("#email").val();
	password = $("#password").val();
	re_password = $("#re_password").val();
	if (password == '') 
	{
		toastr.options = {
		  "closeButton": false,
		  "debug": false,
		  "newestOnTop": false,
		  "progressBar": true,
		  "preventDuplicates": true,
		  "onclick": null,
		  "showDuration": "100",
		  "hideDuration": "1000",
		  "timeOut": "5000",
		  "extendedTimeOut": "1000",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "show",
		  "hideMethod": "hide",
		  "positionClass": "toast-bottom-right",
		};
		toastr.error("Please Fill Password", "Error");
	}
	if (password != re_password) 
	{
		toastr.options = {
		  "closeButton": false,
		  "debug": false,
		  "newestOnTop": false,
		  "progressBar": true,
		  "preventDuplicates": true,
		  "onclick": null,
		  "showDuration": "100",
		  "hideDuration": "1000",
		  "timeOut": "5000",
		  "extendedTimeOut": "1000",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "show",
		  "hideMethod": "hide",
		  "positionClass": "toast-bottom-right",
		};
		toastr.error("Password And Retype Password Are Not Match", "Error");
	}
	else
	{
		$.ajax({
			url: base_url + "login/password_recover",
			type: "POST",
			data: {user_id:user_id, email: email, password: password },
			success: function (result) {
				if (result == "Valid") {
					window.location.href = base_url;
				} 
				else {
					toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "100",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "show",
            "hideMethod": "hide",
            "positionClass": "toast-bottom-right",
	        };
					toastr.error("Email Already", "Error");
				}
			},
		});
	}	
}