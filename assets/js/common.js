base_url = $('base').attr('href');
function show_item_img(item_img,preview_img) {
	var oFReader = new FileReader();
	oFReader.readAsDataURL($("#"+item_img)[0].files[0]);

	oFReader.onload = function (oFREvent) {
		$("#"+preview_img).attr("src", oFREvent.target.result);
	};
}

function update_profile()
{
	name = $("#name").val();
	email = $("#email").val();
	country = $("#country").val();

	var formData = new FormData();
	formData.append("name", name);
	formData.append("email", email);
	formData.append("country", country);
	formData.append("file1", $("#img")[0].files[0]);

	$("#preloader").show();

	$.ajax({
		url: base_url + "user/profile/update_profile",
		type: "POST",
		data: formData,
		processData: false,
		contentType: false,
		success: function (result) {
			$("#preloader").hide();
			if (result == "Valid") {
				location.reload();
				// toastr.options = {
		  //     "closeButton": false,
		  //     "debug": false,
		  //     "newestOnTop": false,
		  //     "progressBar": true,
		  //     "preventDuplicates": true,
		  //     "onclick": null,
		  //     "showDuration": "100",
		  //     "hideDuration": "1000",
		  //     "timeOut": "5000",
		  //     "extendedTimeOut": "1000",
		  //     "showEasing": "swing",
		  //     "hideEasing": "linear",
		  //     "showMethod": "show",
		  //     "hideMethod": "hide",
		  //     "positionClass": "toast-bottom-right",
		  // 	};
				// toastr.success("Profile Update Successfully");
			}
			else
			{
				toastr.options = {
		      "closeButton": false,
		      "debug": false,
		      "newestOnTop": false,
		      "progressBar": true,
		      "preventDuplicates": true,
		      "onclick": null,
		      "showDuration": "100",
		      "hideDuration": "1000",
		      "timeOut": "5000",
		      "extendedTimeOut": "1000",
		      "showEasing": "swing",
		      "hideEasing": "linear",
		      "showMethod": "show",
		      "hideMethod": "hide",
		      "positionClass": "toast-bottom-right",
		  	};
				toastr.error("Something Are Wrong", "Error");
			}
		},
	});
}

function update_password()
{
	password = $('#password').val();
	if (password != '') 
	{
		$.ajax({
			url: base_url + "user/profile/update_password",
			type: "POST",
			data: { password: password },
			success: function (result) {
				if (result == 'Valid') 
				{
					toastr.options = {
			      "closeButton": false,
			      "debug": false,
			      "newestOnTop": false,
			      "progressBar": true,
			      "preventDuplicates": true,
			      "onclick": null,
			      "showDuration": "100",
			      "hideDuration": "1000",
			      "timeOut": "5000",
			      "extendedTimeOut": "1000",
			      "showEasing": "swing",
			      "hideEasing": "linear",
			      "showMethod": "show",
			      "hideMethod": "hide",
			      "positionClass": "toast-bottom-right",
			  	};
					toastr.success("Password Update Successfully");
				}
			},
		});
	}
	else
	{
		toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": true,
      "preventDuplicates": true,
      "onclick": null,
      "showDuration": "100",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "show",
      "hideMethod": "hide",
      "positionClass": "toast-bottom-right",
  	};
		toastr.error("Enter Password", "Error");
	}
}

function register_user_resaller()
{
	name = $("#name").val();
	email = $("#email").val();
	password = $("#password").val();
	re_password = $("#re_password").val();
	if (password == '') 
	{
		toastr.options = {
		  "closeButton": false,
		  "debug": false,
		  "newestOnTop": false,
		  "progressBar": true,
		  "preventDuplicates": true,
		  "onclick": null,
		  "showDuration": "100",
		  "hideDuration": "1000",
		  "timeOut": "5000",
		  "extendedTimeOut": "1000",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "show",
		  "hideMethod": "hide",
		  "positionClass": "toast-bottom-right",
		};
		toastr.error("Please Fill Password", "Error");
	}
	if (password != re_password) 
	{
		toastr.options = {
		  "closeButton": false,
		  "debug": false,
		  "newestOnTop": false,
		  "progressBar": true,
		  "preventDuplicates": true,
		  "onclick": null,
		  "showDuration": "100",
		  "hideDuration": "1000",
		  "timeOut": "5000",
		  "extendedTimeOut": "1000",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "show",
		  "hideMethod": "hide",
		  "positionClass": "toast-bottom-right",
		};
		toastr.error("Password And Retype Password Are Not Match", "Error");
	}
	else
	{
		$.ajax({
			url: base_url + "user/user/save_user",
			type: "POST",
			data: {name:name, email: email, password: password },
			success: function (result) {
				if (result == "Valid") {
					location.reload();
				} 
				else {
					toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "100",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "show",
            "hideMethod": "hide",
            "positionClass": "toast-bottom-right",
	        };
					toastr.error("Email Already", "Error");
				}
			},
		});
	}	
}

function checkPasswordStrength() {
		$('.bt_pass').attr('disabled',true);
    var number = /([0-9])/;
    var alphabets = /([a-zA-Z])/;
    var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
    if ($('#password').val().length < 6) {
    	$('.bt_pass').attr('disabled',true);
        $('#password-strength-status').removeClass();
        $('#password-strength-status').addClass('weak-password');
        $('#password-strength-status').html("Weak (should be atleast 6 characters.)");
    } else {
        if ($('#password').val().match(number) && $('#password').val().match(alphabets) && $('#password').val().match(special_characters)) {
            $('#password-strength-status').removeClass();
            $('#password-strength-status').addClass('strong-password');
            $('#password-strength-status').html("Strong");
            $('.bt_pass').attr('disabled',false);
        } else {
            $('#password-strength-status').removeClass();
            $('#password-strength-status').addClass('medium-password');
            $('#password-strength-status').html("Medium (should include alphabets, numbers and special characters.)");
            $('.bt_pass').attr('disabled',false);
        }
    }
}