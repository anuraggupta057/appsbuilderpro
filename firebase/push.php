<?php

/**
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class Push {

    // push message title
    private $title;
    private $message;
    private $image;
    // push message payload
    private $data;
    // flag indicating whether to show the push
    // notification or not
    // this flag will be useful when perform some opertation
    // in background when push is recevied
    private $is_background;

    function __construct() {
        
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function setImage($imageUrl) {
        $this->image = $imageUrl;
    }

    public function setPayload($data) {
        $this->data = $data;
    }

    public function setIsBackground($is_background) {
        $this->is_background = $is_background;
    }

    public function getPush() {
        $res = array();
        $res['notification']= $this->data;
        // $res['notification']['title'] = $this->title;
        // $res['notification']['is_background'] = $this->is_background;
        // $res['notification']['message'] = $this->message;
        // $res['notification']['image'] = $this->image;
        // $res['notification']['payload'] = $this->data;
        // $res['notification']['timestamp'] = date('Y-m-d G:i:s');
        return $res;
    }

}
